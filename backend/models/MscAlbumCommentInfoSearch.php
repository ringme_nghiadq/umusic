<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\mongodb\ActiveRecord;
use backend\models\Album;
use common\models\MscAlbumCommentInfo;
use common\helpers\Helpers;

class MscAlbumCommentInfoSearch extends MscAlbumCommentInfo
{
    public function rules() {
        return [
            [['name', 'content'], 'string'],
            [['name', 'content'], 'trim'],
            ['contentId', 'integer'],
            ['commentAt', 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'content' => Yii::t('backend', 'Comment Content'),
            'contentId' => Yii::t('backend', 'Album'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);
        $this->load($params);
        $query->andFilterWhere(['LIKE', 'content', $this->content]);
        $query->andFilterWhere(['LIKE', 'name', $this->name]);
        $query->andFilterWhere(['contentId' => $this->contentId]);
        
        if (strpos($this->commentAt, ' - ') > 0) {
            $dates = Helpers::splitDate($this->commentAt, 'd/m/Y');
            $query->andFilterWhere(['>=', 'commentAt', Helpers::toIsoDate(strtotime($dates[0]))]);
            $query->andFilterWhere(['<=', 'commentAt', Helpers::toIsoDate(strtotime($dates[1]))]);
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    public function getContentName() {
        $album = Album::findOne($this->contentId);
        
        if($album) {
            return Html::a($album->album_name, Url::to(['album/song', 'id' => $this->contentId]), ['data-pjax' => 'xxx']);
        }

        return null;
    }
}
