<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

class MscCategory extends \common\models\MscCategoryBase {

    public $selected_countries  = [];

    public function loadDefaultValues($skipIfSet = true) {
        if (!$this->isNewRecord) {
            $this->selected_countries = ArrayHelper::map($this->getCountryCodes(), 'country_code', 'country_code');
        }

        return parent::loadDefaultValues($skipIfSet);
    }

    public function getCountryCodes()
    {
        return MscCategoryCountry::find()
            ->where(
                ['cate_id' => $this->id]
            )
            ->all();
    }


    public function saveCountries()
    {
        /* clear the countries of the category before saving */
        MscCategoryCountry::deleteAll(['cate_id' => $this->id]);
        if (is_array($this->selected_countries)) {
            foreach($this->selected_countries as $selected_country) {
                $cc = new MscCategoryCountry();
                $cc->cate_id = $this->id;
                $cc->country_code = $selected_country;
                $cc->save();
            }
        }
    }

    public function rules()
    {
        return [
            [['cate_name', 'is_active'], 'required'],
            [['description'], 'string'],
            [['is_active'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['cate_name', 'slug'], 'string', 'max' => 255],
            [['avatar', 'banner'], 'string', 'max' => 500],

            [['cate_name'], 'unique'],
            [['selected_countries'], 'safe'],
            [['avatar', 'banner', 'cate_name', 'description', 'updated_at', 'created_at'], 'trim'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'cate_name' => Yii::t('backend', 'Name'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'banner' => Yii::t('backend', 'Banner'),
            'description' => Yii::t('backend', 'Description'),
            'is_active' => Yii::t('backend', 'Status'),
            'slug' => Yii::t('backend', 'Slug'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    public function getCountries() {
        return $this->hasMany(MscCountry::className(), ['country_code' => 'country_code'])
            ->viaTable('msc_category_country', ['cate_id' => 'id'])
            ->orderBy('name asc')
            ;
    }
}