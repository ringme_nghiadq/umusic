<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Playlist;

/**
 * PlaylistSearch represents the model behind the search form about `backend\models\Playlist`.
 */
class PlaylistSearch extends Playlist
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'number_of_songs', 'total_listens', 'total_download', 'total_liked', 'total_shared', 'total_comment', 'boost_score', 'number_of_artists'], 'integer'],
            [['playlist_name', 'avatar', 'banner', 'thumbnail', 'description', 'is_active', 'slug', 'belong', 'userId', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Playlist::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'number_of_songs' => $this->number_of_songs,
            'total_listens' => $this->total_listens,
            'total_download' => $this->total_download,
            'total_liked' => $this->total_liked,
            'total_shared' => $this->total_shared,
            'total_comment' => $this->total_comment,
            'boost_score' => $this->boost_score,
            'number_of_artists' => $this->number_of_artists,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'playlist_name', $this->playlist_name])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'banner', $this->banner])
            ->andFilterWhere(['like', 'thumbnail', $this->thumbnail])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'is_active', $this->is_active])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'belong', $this->belong])
            ->andFilterWhere(['like', 'userId', $this->userId]);

        return $dataProvider;
    }
}
