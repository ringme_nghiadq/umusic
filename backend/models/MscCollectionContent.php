<?php

namespace backend\models;

use Yii;
use common\models\PlaylistBase;
use common\models\AlbumBase;
use common\models\MscCategoryBase;

class MscCollectionContent extends \common\models\MscCollectionContentBase {
    public function getContentName() {
        $contentName = null;
        switch ($this->content_type) {
            case self::CONTENT_TYPE_PLAYLIST:
                $model = PlaylistBase::findOne($this->content_id);
                $contentName = $model ? $model->playlist_name : $contentName;
                if($contentName) {
                    $contentName = Html::a($contentName, Url::to(['playlist/song', 'id' => $this->content_id]), ['data-pjax' => 'xxx']);
                }
                break;
            case self::CONTENT_TYPE_ALBUM:
                $model = AlbumBase::findOne($this->content_id);
                $contentName = $model ? $model->album_name : $contentName;
                if($contentName) {
                    $contentName = Html::a($contentName, Url::to(['album/song', 'id' => $this->content_id]), ['data-pjax' => 'xxx']);
                }
                break;
            case self::CONTENT_TYPE_CATEGORY:
                $model = MscCategoryBase::findOne($this->content_id);
                $contentName = $model ? $model->cate_name : $contentName;
                if($contentName) {
                    $contentName = Html::a($contentName, Url::to(['msc-category/update', 'id' => $this->content_id]), ['data-pjax' => 'xxx']);
                }
                break;
        }
        return $contentName;
    }
}