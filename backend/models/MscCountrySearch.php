<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MscCountry;

/**
 * MscCountrySearch represents the model behind the search form about `backend\models\MscCountry`.
 */
class MscCountrySearch extends MscCountry
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['country_code', 'country_name', 'country_name_en', 'flag_image'], 'safe'],
            [['country_code', 'country_name', 'country_name_en', 'flag_image'], 'trim'],
            [['country_code', 'country_name', 'country_name_en', 'flag_image'], 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MscCountry::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'country_code', $this->country_code])
            ->andFilterWhere(['like', 'country_name', $this->country_name])
            ->andFilterWhere(['like', 'country_name_en', $this->country_name_en])
            ->andFilterWhere(['like', 'flag_image', $this->flag_image]);

        return $dataProvider;
    }
}
