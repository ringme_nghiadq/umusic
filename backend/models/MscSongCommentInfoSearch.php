<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\mongodb\ActiveRecord;
use backend\models\Song;
use common\models\MscSongCommentInfo;
use common\helpers\Helpers;

class MscSongCommentInfoSearch extends MscSongCommentInfo
{
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'msc_song_comment_info';
    }

    public function rules() {
        return [
            [['name', 'content'], 'string'],
            [['name', 'content'], 'trim'],
            ['contentId', 'integer'],
            ['commentAt', 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'content' => Yii::t('backend', 'Comment Content'),
            'contentId' => Yii::t('backend', 'Song'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);
        $this->load($params);
        $query->andFilterWhere(['LIKE', 'content', $this->content]);
        $query->andFilterWhere(['LIKE', 'name', $this->name]);
        $query->andFilterWhere(['contentId' => $this->contentId]);
        
        if (strpos($this->commentAt, ' - ') > 0) {
            $dates = Helpers::splitDate($this->commentAt, 'd/m/Y');
            $query->andFilterWhere(['>=', 'commentAt', Helpers::toIsoDate(strtotime($dates[0]))]);
            $query->andFilterWhere(['<=', 'commentAt', Helpers::toIsoDate(strtotime($dates[1]))]);
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    public function getContentName() {
        $song = Song::findOne($this->contentId);
        
        // if($song) {
        //     return Html::a($song->song_name, Url::to(['song/u', 'id' => $this->contentId]), ['data-pjax' => 'xxx']);
        // }

        return $song ? $song->song_name : null;
    }
}
