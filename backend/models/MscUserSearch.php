<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MscUser;

/**
 * MscUserSearch represents the model behind the search form about `backend\models\MscUser`.
 */
class MscUserSearch extends MscUser
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['user_id', 'login_id', 'base_on', 'phone_number', 'email', 'password', 'token', 'user_name', 'active', 'token_expired', 'created_at', 'revision', 'client_type', 'device_id', 'device_name', 'app_version', 'os_version', 'uuid', 'regid', 'v_regid', 'last_login', 'otp', 'otp_expired', 'link_reset_password', 'token_reset_password_expired'], 'safe'],
            [['user_id', 'login_id', 'base_on', 'phone_number', 'email', 'password', 'token', 'user_name', 'active', 'token_expired', 'created_at', 'revision', 'client_type', 'device_id', 'device_name', 'app_version', 'os_version', 'uuid', 'regid', 'v_regid', 'last_login', 'otp', 'otp_expired', 'link_reset_password', 'token_reset_password_expired'], 'trim'],
            [['user_id', 'login_id', 'base_on', 'phone_number', 'email', 'password', 'token', 'user_name', 'active', 'token_expired', 'created_at', 'revision', 'client_type', 'device_id', 'device_name', 'app_version', 'os_version', 'uuid', 'regid', 'v_regid', 'last_login', 'otp', 'otp_expired', 'link_reset_password', 'token_reset_password_expired'], 'filter', 'filter' => 'trim'],
            [['last_avatar'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MscUser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,

        ]);

        $query->andFilterWhere(['like', 'user_id', $this->user_id])

            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'email', $this->email])

            ->andFilterWhere(['like', 'user_name', $this->user_name])
        ;

        if ($this->created_at != Yii::t('backend', 'All') && strpos($this->created_at, ' - ') > 0) {
            $request_times = \common\helpers\Helpers::splitDate($this->created_at, 'd/m/Y');
            $query->andFilterWhere(['BETWEEN', 'created_at', $request_times[0], $request_times[1]]);
        }

        if ($this->active != '-1') {
            $query->andFilterWhere(['!=', 'active' , -1]);
        }
        $query->andFilterWhere([
            'active' => $this->active,
        ]);

        return $dataProvider;
    }
}
