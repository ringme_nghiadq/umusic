<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Album;

/**
 * AlbumSearch represents the model behind the search form about `backend\models\Album`.
 */
class AlbumSearch extends Album
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'number_of_songs', 'total_listens', 'total_download', 'total_liked', 'total_shared', 'total_comment', 'number_of_artists'], 'integer'],
            [['country_code', 'album_name', 'avatar', 'banner', 'thumbnail', 'description', 'is_active', 'slug', 'boost_score', 'published_time', 'updated_at', 'created_at'], 'safe'],
            [['country_code', 'album_name', 'avatar', 'banner', 'thumbnail', 'description', 'is_active', 'slug', 'boost_score', 'published_time', 'updated_at', 'created_at'], 'trim'],
            [['country_code', 'album_name', 'avatar', 'banner', 'thumbnail', 'description', 'is_active', 'slug', 'boost_score', 'published_time', 'updated_at', 'created_at'], 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Album::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,

        ]);

        $query->andFilterWhere(['like', 'country_code', $this->country_code])
            ->andFilterWhere(['like', 'album_name', $this->album_name])
           ;

        if ($this->published_time != Yii::t('backend', 'All') && strpos($this->published_time, ' - ') > 0) {
            $request_times = \common\helpers\Helpers::splitDate($this->published_time, 'd/m/Y');
            $query->andFilterWhere(['BETWEEN', 'published_time', $request_times[0], $request_times[1]]);
        }

        if ($this->is_active != '-1') {
            $query->andFilterWhere(['!=', 'is_active' , -1]);
        }
        $query->andFilterWhere([
            'is_active' => $this->is_active,
        ]);

        return $dataProvider;
    }
}
