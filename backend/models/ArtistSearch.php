<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Artist;

/**
 * ArtistSearch represents the model behind the search form about `backend\models\Artist`.
 */
class ArtistSearch extends Artist
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['country_code', 'alias_name', 'real_name', 'is_active', 'banner', 'avatar', 'thumbnail', 'info', 'birthday', 'gender', 'address', 'slug', 'boost_score', 'updated_at', 'created_at'], 'safe'],
            [['country_code', 'alias_name', 'real_name', 'is_active', 'banner', 'avatar', 'thumbnail', 'info', 'birthday', 'gender', 'address', 'slug', 'boost_score', 'updated_at', 'created_at'], 'trim'],
            [['country_code', 'alias_name', 'real_name', 'is_active', 'banner', 'avatar', 'thumbnail', 'info', 'birthday', 'gender', 'address', 'slug', 'boost_score', 'updated_at', 'created_at'], 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Artist::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,

        ]);

        $query->andFilterWhere(['like', 'country_code', $this->country_code])
            ->andFilterWhere(['like', 'alias_name', $this->alias_name])
            ->andFilterWhere(['like', 'real_name', $this->real_name])

            ->andFilterWhere(['like', 'gender', $this->gender])
            ;

        if ($this->is_active != '-1') {
            $query->andFilterWhere(['!=', 'is_active' , -1]);
        }
        $query->andFilterWhere([
            'is_active' => $this->is_active,
        ]);

        return $dataProvider;
    }
}
