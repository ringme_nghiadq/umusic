<?php

namespace backend\models;

use Yii;

class Author extends \common\models\AuthorBase {

    public function rules()
    {
        return [
            [['country_code', 'alias_name'], 'required'],
            [['info'], 'string'],
            [['birthday', 'updated_at', 'created_at'], 'safe'],
            [['country_code'], 'string', 'max' => 10],
            [['alias_name', 'real_name', 'address', 'slug'], 'string', 'max' => 255],
            [['avatar', 'thumbnail'], 'string', 'max' => 500],
            [['is_active'], 'string', 'max' => 1],
            [['gender'], 'string', 'max' => 5]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'country_code' => Yii::t('backend', 'Country'),
            'alias_name' => Yii::t('backend', 'Alias Name'),
            'real_name' => Yii::t('backend', 'Real Name'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'thumbnail' => Yii::t('backend', 'Thumbnail'),
            'is_active' => Yii::t('backend', 'Status'),
            'info' => Yii::t('backend', 'Info'),
            'birthday' => Yii::t('backend', 'Birthday'),
            'gender' => Yii::t('backend', 'Gender'),
            'address' => Yii::t('backend', 'Address'),
            'slug' => Yii::t('backend', 'Slug'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}