<?php

namespace backend\models;

use Yii;

class MscCollection extends \common\models\MscCollectionBase {

    public function rules()
    {
        return [
            [['collection_name'], 'required'],
            [['description'], 'string'],
            [['is_active', 'number_of_artists'], 'integer'],
            [['is_active', 'updated_at', 'created_at'], 'safe'],
            [['collection_name'], 'string', 'max' => 255],
            [['note', 'description'], 'string', 'max' => 500]
        ];
    }

}