<?php

namespace backend\models;

use Yii;

class MscCountry extends \common\models\MscCountryBase {

    public function rules()
    {
        return [
            [['country_code'], 'unique'],
            [['country_name'], 'unique'],

            [['country_code'], 'string', 'max' => 10],
            [['country_name', 'country_name_en'], 'string', 'max' => 255],
            [['flag_image'], 'string', 'max' => 500],
        ];
    }
}