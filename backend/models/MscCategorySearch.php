<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MscCategory;

/**
 * MscCategorySearch represents the model behind the search form about `backend\models\MscCategory`.
 */
class MscCategorySearch extends MscCategory
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_active'], 'integer'],
            [['cate_name', 'avatar', 'banner', 'description', 'slug', 'updated_at', 'created_at'], 'safe'],
            [['cate_name', 'avatar', 'banner', 'description', 'slug', 'updated_at', 'created_at'], 'trim'],
            [['cate_name', 'avatar', 'banner', 'description', 'slug', 'updated_at', 'created_at'], 'filter', 'filter' => 'trim'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MscCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,

        ]);

        $query->andFilterWhere(['like', 'cate_name', $this->cate_name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ;

        if ($this->is_active != '-1') {
            $query->andFilterWhere(['!=', 'is_active' , -1]);
        }
        $query->andFilterWhere([
            'is_active' => $this->is_active,
        ]);

        return $dataProvider;
    }
}
