<?php

namespace backend\models;

use Yii;
use backend\models\MscCollectionContent;
use backend\models\SongAlbum;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;

class Album extends \common\models\AlbumBase {

    public $selected_collections = [];
    public $selected_songs =[];

    public function loadDefaultValues($skipIfSet = true) {
        if (!$this->isNewRecord) {
            $this->selected_collections = ArrayHelper::map($this->getCollectionIds(), 'collection_id', 'collection_id');
            $this->selected_songs = ArrayHelper::map($this->getSongIds(), 'song_id', 'song_id');
        }

        return parent::loadDefaultValues($skipIfSet);
    }

    public function getCollectionIds()
    {
        return MscCollectionContent::find()
            ->select('collection_id')
            ->where(
                ['content_id' => $this->id, 'content_type' => 'album']
            )
            ->all();
    }

    public function getSongIds()
    {
        return SongAlbum::find()
            ->select('song_id')
            ->where(
                ['album_id' => $this->id,]
            )
            ->all();
    }

    public function save($runValidation = true, $attributeNames = NULL) {

        if(!$this->isNewRecord) {
            $currentIds = $this->getCollectionIds();
            $currentIds1 = $this->getSongIds();

            if (count($currentIds)) {
                foreach ($currentIds as $index => $collection) {

                    // Neu truoc do da chon --> xoa bot di
                    $exists = false;
                    if (is_array($this->selected_collections) && count($this->selected_collections)) {
                        foreach ($this->selected_collections as $selectedId) {
                            if($selectedId == $collection->collection_id) {
                                $exists = true;
                                break;
                            }
                        }

                        // neu bo chon --> xoa di
                        if($exists === false) {
                            MscCollectionContent::deleteAll([
                                'content_id' => $this->id,
                                'content_type' => 'album',
                                'collection_id' => $collection->collection_id,
                            ]);

                        }
                    } else {

                        MscCollectionContent::deleteAll([
                            'content_id' => $this->id,
                            'content_type' => 'album',
                        ]);
                    }

                }
            }

            if (count($currentIds1)) {
                foreach ($currentIds1 as $index => $songs) {

                    // Neu truoc do da chon --> xoa bot di
                    $exists = false;
                    if (is_array($this->selected_songs) && count($this->selected_songs)) {
                        foreach ($this->selected_songs as $selectedId) {
                            if($selectedId == $songs->song_id) {
                                $exists = true;
                                break;
                            }
                        }

                        // neu bo chon --> xoa di
                        if($exists === false) {
                            SongAlbum::deleteAll([
                                'album_id' => $this->id,
                                'song_id' => $songs->song_id,
                            ]);

                        }
                    } else {

                        SongAlbum::deleteAll([
                            'album_id' => $this->id,
                        ]);
                    }

                }
            }

            // them nhung partner moi
            if (is_array($this->selected_collections) && count($this->selected_collections)) {
                foreach ($this->selected_collections as $item) {
                    // tag co san
                    $exists = false;

                    foreach ($currentIds as $collection) {
                        if($collection->collection_id == $item) {
                            $exists = true;
                            break;
                        }
                    }

                    // neu post chua co tag nay thi gan tag nay vao post
                    if($exists === false) {
                        $tag = new MscCollectionContent();
                        $tag->content_id = $this->id;
                        $tag->collection_id = $item;
                        $tag->content_type = 'album';
                        $tag->save();
                    }

                }
            }

            if (is_array($this->selected_songs) && count($this->selected_songs)) {
                foreach ($this->selected_songs as $item) {
                    // tag co san
                    $exists = false;

                    foreach ($currentIds1 as $song) {
                        if($song->song_id == $item) {
                            $exists = true;
                            break;
                        }
                    }

                    // neu post chua co tag nay thi gan tag nay vao post
                    if($exists === false) {
                        $tag = new SongAlbum();
                        $tag->album_id = $this->id;
                        $tag->song_id = $item;
                        $tag->save();
                    }

                }
            }


            return parent::save($runValidation, $attributeNames);

        } else {
            $return = parent::save($runValidation, $attributeNames);
            if (is_array($this->selected_collections) && count($this->selected_collections)) {
                foreach ($this->selected_collections as $item) {
                    // tag co san
                    if ($item) {
                        $tag = new MscCollectionContent();
                        $tag->content_id = $this->id;
                        $tag->collection_id = $item;
                        $tag->content_type = 'album';
                        $tag->save();
                    }
                }
            }

            if (is_array($this->selected_songs) && count($this->selected_songs)) {
                foreach ($this->selected_songs as $item) {
                    // tag co san
                    if ($item) {
                        $tag = new SongAlbum();
                        $tag->album_id = $this->id;
                        $tag->song_id = $item;
                        $tag->save();
                    }
                }
            }

            return $return;
        }
    }

    public function rules()
    {
        return [
            [['album_name',], 'required'],
            [['description'], 'string'],
            [['number_of_songs', 'total_listens', 'total_download', 'total_liked', 'total_shared', 'total_comment', 'number_of_artists'], 'integer'],
            [['published_time', 'updated_at', 'created_at'], 'safe'],
            [['country_code'], 'string', 'max' => 10],
            [['album_name', 'avatar', 'banner', 'thumbnail'], 'string', 'max' => 500],
            [['is_active'], 'string', 'max' => 1],
            [['slug'], 'string', 'max' => 255],
            [['boost_score'], 'double'],

            [['album_name', 'description'], 'trim'],

            [['selected_collections', 'selected_songs'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'country_code' => Yii::t('backend', 'Country'),
            'album_name' => Yii::t('backend', 'Album Name'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'banner' => Yii::t('backend', 'Banner'),
            'thumbnail' => Yii::t('backend', 'Thumbnail'),
            'description' => Yii::t('backend', 'Description'),
            'is_active' => Yii::t('backend', 'Status'),
            'number_of_songs' => Yii::t('backend', 'Number Of Songs'),
            'slug' => Yii::t('backend', 'Slug'),
            'total_listens' => Yii::t('backend', 'Total Listens'),
            'total_download' => Yii::t('backend', 'Total Download'),
            'total_liked' => Yii::t('backend', 'Total Liked'),
            'total_shared' => Yii::t('backend', 'Total Shared'),
            'total_comment' => Yii::t('backend', 'Total Comment'),
            'boost_score' => Yii::t('backend', 'Boost Score'),
            'number_of_artists' => Yii::t('backend', 'Number Of Artists'),
            'published_time' => Yii::t('backend', 'Published Time'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    public function getCollections() {
        return $this->hasMany(MscCollection::className(), ['id' => 'collection_id'])
            ->viaTable('msc_collection_content', ['content_id' => 'id'], ['content_type' => 'album'])
            ->orderBy('collection_id asc')
            ;
    }


}

