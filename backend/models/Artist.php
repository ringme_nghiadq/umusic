<?php

namespace backend\models;

use Yii;

class Artist extends \common\models\ArtistBase {

    public function rules()
    {
        return [
            [['country_code', 'alias_name',], 'required'],
            [['info', 'boost_score'], 'string'],
            [['birthday', 'updated_at', 'created_at'], 'safe'],
            [['country_code'], 'string', 'max' => 10],
            [['alias_name', 'real_name', 'address', 'slug'], 'string', 'max' => 255],
            [['is_active'], 'string', 'max' => 1],
            [['banner', 'avatar', 'thumbnail'], 'string', 'max' => 500],
            [['gender'], 'in', 'range' => array_keys(parent::getAllGenders())],
            [['boost_score'], 'double'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'country_code' => Yii::t('backend', 'Country'),
            'alias_name' => Yii::t('backend', 'Alias Name'),
            'real_name' => Yii::t('backend', 'Real Name'),
            'is_active' => Yii::t('backend', 'Status'),
            'banner' => Yii::t('backend', 'Banner'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'thumbnail' => Yii::t('backend', 'Thumbnail'),
            'info' => Yii::t('backend', 'Info'),
            'birthday' => Yii::t('backend', 'Birthday'),
            'gender' => Yii::t('backend', 'Gender'),
            'address' => Yii::t('backend', 'Address'),
            'slug' => Yii::t('backend', 'Slug'),
            'boost_score' => Yii::t('backend', 'Boost Score'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}