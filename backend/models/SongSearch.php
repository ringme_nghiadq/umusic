<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Song;

/**
 * SongSearch represents the model behind the search form about `backend\models\Song`.
 */
class SongSearch extends Song
{
    public $artist_id;
    public $category_id;
    public $collection_type;
    public $show_deleted_songs;

    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id', 'bit_rate', 'duration', 'is_download', 'total_listens', 'total_download', 'total_liked', 'total_shared', 'total_comment', 'total_score', 'number_of_artists'], 'integer'],
            [['country_code', 'song_name', 'avatar', 'banner', 'thumbnail', 'media_path', 'lyric', 'is_active', 'slug', 'published_time', 'updated_at', 'created_at'], 'safe'],
            [['country_code', 'song_name', 'published_time', 'updated_at', 'created_at'], 'trim'],
            [['country_code', 'song_name', 'published_time', 'updated_at', 'created_at'], 'filter', 'filter' => 'trim'],
            [['artist_id', 'category_id', 'collection_type', 'show_deleted_songs'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Song::find()->alias('a');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->category_id) {
            $query
                -> innerJoin('msc_song_category c', '(c.song_id=a.id AND c.cate_id = :cateId)', [
                    'cateId' => $this->category_id,
                ]);
        }

        if ($this->artist_id) {
            $query->innerJoin('msc_song_artist sa', 'sa.song_id = a.id')->andFilterWhere([
                'sa.artist_id' => $this->artist_id,
            ]);
        }

        switch ($this->collection_type) {
            case 'ranking_tab':
                $query->orderBy('a.total_score DESC');
                break;
            case 'new_tab':
                $query->orderBy('a.created_at DESC');
                break;
            default:

        }

        $query->andFilterWhere([
            'a.id' => $this->id,
            'a.author_id' => $this->author_id,
            'a.is_download' => $this->is_download,
        ]);

        $query->andFilterWhere(['like', 'a.country_code', $this->country_code])
            ->andFilterWhere(['like', 'a.song_name', $this->song_name])

        ;

        if ($this->published_time != Yii::t('backend', 'All') && strpos($this->published_time, ' - ') > 0) {
            $request_times = \common\helpers\Helpers::splitDate($this->published_time, 'd/m/Y');
            $query->andFilterWhere(['BETWEEN', 'a.published_time', $request_times[0], $request_times[1]]);
        }

        if ($this->is_active != '-1') {
            $query->andFilterWhere(['!=', 'a.is_active' , self::STATUS_DELETE]);
        }
        $query->andFilterWhere([
            'a.is_active' => $this->is_active,
        ]);
        $query->orderBy(['updated_at' => SORT_DESC]);
        return $dataProvider;
    }
}
