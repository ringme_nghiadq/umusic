<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RepTopListening;
use common\helpers\Helpers;

/**
 * RepTopListeningSearch represents the model behind the search form about `backend\models\RepTopListening`.
 */
class RepTopListeningSearch extends RepTopListening
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'song_id', 'total_listens'], 'integer'],
            [['report_date'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepTopListening::find()->with('song')->limit(100)->orderBy(['total_listens' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        if (strpos($this->report_date, ' - ') > 0) {
            $dates = Helpers::splitDate($this->report_date, 'd/m/Y');
            $query->andFilterWhere(['BETWEEN', 'report_date', $dates[0], $dates[1]]);
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'song_id' => $this->song_id,
            'total_listens' => $this->total_listens,
        ]);
        return $dataProvider;
    }
}
