<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MscCollectionContent;

/**
 * MscCollectionContentSearch represents the model behind the search form about `backend\models\MscCollectionContent`.
 */
class MscCollectionContentSearch extends MscCollectionContent
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'collection_id', 'content_id'], 'integer'],
            [['content_type', 'created_at'], 'safe'],
            ['collection_id', 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MscCollectionContent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'collection_id' => $this->collection_id,
            'content_id' => $this->content_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'content_type', $this->content_type]);

        return $dataProvider;
    }
}
