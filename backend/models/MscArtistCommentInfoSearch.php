<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\MscArtistCommentInfo;

class MscArtistCommentInfoSearch extends MscArtistCommentInfo
{
    public function rules() {
        return [
            [['name', 'content'], 'string'],
            [['name', 'content'], 'trim'],
            ['contentId', 'integer'],
            ['commentAt', 'safe']
        ];
    }

    public function attributeLabels() {
        return [
            'content' => Yii::t('backend', 'Comment Content'),
            'contentId' => Yii::t('backend', 'Artist'),
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);
        $this->load($params);
        $query->andFilterWhere(['LIKE', 'content', $this->content]);
        $query->andFilterWhere(['LIKE', 'name', $this->name]);
        $query->andFilterWhere(['contentId' => $this->contentId]);
        
        if (strpos($this->commentAt, ' - ') > 0) {
            $dates = Helpers::splitDate($this->commentAt, 'd/m/Y');
            $query->andFilterWhere(['>=', 'commentAt', Helpers::toIsoDate(strtotime($dates[0]))]);
            $query->andFilterWhere(['<=', 'commentAt', Helpers::toIsoDate(strtotime($dates[1]))]);
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    public function getContentName() {
        $artist = Artist::findOne($this->contentId);
        return $artist ? $artist->alias_name : null;
    }
}
