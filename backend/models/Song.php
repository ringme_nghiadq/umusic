<?php

namespace backend\models;

use frontend\models\SongCategory;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;

class Song extends \common\models\SongBase {

    public $selected_categories  = [];
    public $selected_artists  = [];

    public function loadDefaultValues($skipIfSet = true) {
        if (!$this->isNewRecord) {
            $this->selected_categories = ArrayHelper::map($this->getCategoryIds(), 'cate_id', 'cate_id');
            $this->selected_artists = ArrayHelper::map($this->getArtistIds(), 'artist_id', 'artist_id');
        }

        return parent::loadDefaultValues($skipIfSet);
    }

    public function getArtistIds()
    {
        return SongArtist::find()
            ->select('artist_id')
            ->where(
                ['song_id' => $this->id]
            )
            ->all();
    }

    public function getCategoryIds()
    {
        return SongCategory::find()
            ->select('cate_id')
            ->where(
                ['song_id' => $this->id]
            )
            ->all();
    }

    public function afterSave($insert, $changedAttributes)
    {
        /**
         * Them/xoa artist
         */
        $currentSongArtists = ArrayHelper::map($this->getArtistIds(), 'artist_id', 'artist_id');

        $insertArtistIds = [];
        $deleteArtistIds = [];

        if (!is_array($this->selected_artists) || !count($this->selected_artists)) {
            // ko chon thang nao
            $insertArtistIds = [];
            $deleteArtistIds = $currentSongArtists;

        } elseif (is_array($this->selected_artists) && count($this->selected_artists)) {

            $insertArtistIds = array_diff($this->selected_artists, $currentSongArtists);
            $deleteArtistIds = array_diff($currentSongArtists, $this->selected_artists);
        }

        if (count($insertArtistIds)) {
            foreach ($insertArtistIds as $id) {
                $m = new SongArtist();
                $m->song_id = $this->id;
                $m->artist_id = $id;
                $m->save();
            }
        }

        if (count($deleteArtistIds)) {
            SongArtist::deleteAll([
                'song_id' => $this->id,
                'artist_id' => $deleteArtistIds,
            ]);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function save($runValidation = true, $attributeNames = NULL) {

        if(!$this->isNewRecord) {
            $currentIds = $this->getCategoryIds();

            if (count($currentIds)) {
                foreach ($currentIds as $index => $cate) {

                    // Neu truoc do da chon --> xoa bot di
                    $exists = false;
                    if (is_array($this->selected_categories) && count($this->selected_categories)) {
                        foreach ($this->selected_categories as $selectedId) {
                            if($selectedId == $cate->cate_id) {
                                $exists = true;
                                break;
                            }
                        }

                        // neu bo chon --> xoa di
                        if($exists === false) {
                            SongCategory::deleteAll([
                                'song_id' => $this->id,
                                'cate_id' => $cate->cate_id,
                            ]);

                        }
                    } else {

                        SongCategory::deleteAll([
                            'song_id' => $this->id,
                        ]);
                    }

                }
            }

            // them nhung partner moi
            if (is_array($this->selected_categories) && count($this->selected_categories)) {
                foreach ($this->selected_categories as $item) {
                    // tag co san
                    $exists = false;

                    foreach ($currentIds as $cate) {
                        if($cate->cate_id == $item) {
                            $exists = true;
                            break;
                        }
                    }

                    // neu post chua co tag nay thi gan tag nay vao post
                    if($exists === false) {
                        $tag = new SongCategory();
                        $tag->song_id = $this->id;
                        $tag->cate_id = $item;
                        $tag->save();
                    }

                }
            }


            return parent::save($runValidation, $attributeNames);

        } else {
            $return = parent::save($runValidation, $attributeNames);
            if (is_array($this->selected_categories) && count($this->selected_categories)) {
                foreach ($this->selected_categories as $item) {
                    // tag co san
                    if ($item) {
                        $tag = new SongCategory();
                        $tag->song_id = $this->id;
                        $tag->cate_id = $item;
                        $tag->save();
                    }
                }
            }

            return $return;
        }
    }

    /**
     * Upload song
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\base\Exception
     */
    public function uploadMedia()
    {
        if ($this->validate() && $this->media_path) {

            $absSavePath = $this->getAbsMediaPath();

            $fileName = md5($this->id.'_'. time(). Inflector::camel2id($this->media_path->name)). '.'. $this->media_path->extension;

            $fullFilePath = Yii::$app->params['media_path'] . $absSavePath;

            if (!file_exists($fullFilePath)) {
                FileHelper::createDirectory($fullFilePath);
            }

            $this->media_path->saveAs($fullFilePath . DIRECTORY_SEPARATOR . $fileName);
            $this->media_path = $absSavePath . $fileName;

            $this->update(false, ['media_path']);

            return true;
        } else {

            return false;
        }
    }

    public function getAbsMediaPath() {
        return '/cms_upload/song/audio/'. date('Y/m/d/');
    }

    public function rules()
    {
        $rules = [
            [['author_id', 'bit_rate', 'duration', 'is_download', 'total_listens', 'total_download', 'total_liked', 'total_shared', 'total_comment', 'total_score', 'number_of_artists'], 'integer'],
            [['bit_rate', 'duration', 'is_download', 'total_listens', 'total_download', 'total_liked', 'total_shared', 'total_comment', 'total_score', 'number_of_artists'], 'safe'],
            [['country_code', 'song_name'], 'required'],
            [['lyric'], 'string'],
            [['published_time', 'updated_at', 'created_at'], 'safe'],
            [['country_code'], 'string', 'max' => 10],
            [['song_name', 'slug'], 'string', 'max' => 255],
            [['avatar', 'banner', 'thumbnail'], 'string', 'max' => 500],
            [['is_active'], 'string', 'max' => 1],

            [['selected_categories'], 'safe'],
            [['selected_artists'], 'safe'],
        ];

        if ($this->isNewRecord) {
            $rules = array_merge($rules, [
                [['media_path'], 'file', 'skipOnEmpty' => false, 'extensions' => 'mp3', 'maxSize' => 512000000, 'tooBig' => Yii::t('backend', 'File is too big, maximun is 512Mb') ],
                ['media_path', 'required', 'message' => Yii::t('backend', 'Please upload audio')]
            ]);
        } else {
            $rules = array_merge($rules, [
                [['media_path'], 'file', 'skipOnEmpty' => true, 'extensions' => 'mp3',],
            ]);
        }
        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'author_id' => Yii::t('backend', 'Author'),
            'country_code' => Yii::t('backend', 'Country'),
            'song_name' => Yii::t('backend', 'Song Name'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'banner' => Yii::t('backend', 'Banner'),
            'thumbnail' => Yii::t('backend', 'Thumbnail'),
            'media_path' => Yii::t('backend', 'Media'),
            'lyric' => Yii::t('backend', 'Lyric'),
            'is_active' => Yii::t('backend', 'Status'),
            'bit_rate' => Yii::t('backend', 'Bit Rate'),
            'duration' => Yii::t('backend', 'Duration'),
            'slug' => Yii::t('backend', 'Slug'),
            'is_download' => Yii::t('backend', 'Is Download'),
            'total_listens' => Yii::t('backend', 'Total Listens'),
            'total_download' => Yii::t('backend', 'Total Download'),
            'total_liked' => Yii::t('backend', 'Total Liked'),
            'total_shared' => Yii::t('backend', 'Total Shared'),
            'total_comment' => Yii::t('backend', 'Total Comment'),
            'total_score' => Yii::t('backend', 'Total Score'),
            'number_of_artists' => Yii::t('backend', 'Number Of Artists'),
            'published_time' => Yii::t('backend', 'Published Time'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }

    public static function getSongArray() {
        $itemArr = self::find()
            ->select('id, song_name')
            ->asArray()
            ->orderBy('song_name asc')
            ->all();

        if (!empty($itemArr)) {
            return  \yii\helpers\ArrayHelper::map($itemArr, 'id', 'song_name');

        } else {
            return array();
        }
    }

    public function getCategories() {
        return $this->hasMany(MscCategory::className(), ['id' => 'cate_id'])
            ->viaTable('msc_song_category', ['song_id' => 'id'])
            ->orderBy('name asc')
            ;
    }

    public function getArtists() {
        return $this->hasMany(Artist::className(), ['id' => 'artist_id'])
            ->viaTable('msc_song_artist', ['song_id' => 'id'])
            ->orderBy('alias_name asc')
            ;
    }

}