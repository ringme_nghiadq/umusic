<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

class Partner extends \common\models\PartnerBase {
    public $selected_branches  = [];

    public function rules()
    {
        return [
            [['name', 'ruc'], 'required'],
            [['name'], 'unique'],
            [['ruc'], 'unique'],
            [['ruc'], 'match', 'pattern' => Yii::$app->params['partner_ruc_pattern'],
                'message' => Yii::t('backend', 'RUC only allows numbers')
                ],
            [['created_at', 'updated_at', 'selected_branches', 'name_title'], 'safe'],
            [['name', 'ruc'], 'string', 'max' => 200],
            [['director', 'director_dni', 'address'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            [['status'], 'string', 'max' => 1],

            [['province_id', 'district_id', 'precinct_id', 'address_text'], 'safe'],
            [['province_id', 'district_id', 'precinct_id', 'address_text'], 'string'],


            [['msisdn', 'msisdn_warning'], 'required'],
            [['msisdn', 'msisdn_warning'], 'string'],
            [['msisdn'], 'match', 'pattern' => Yii::$app->params['phonenumber_pattern']],
            [['msisdn_warning'], 'match', 'pattern' => Yii::$app->params['phonenum_list_pattern']],

        ];
    }

    public function loadDefaultValues($skipIfSet = true) {
        if (!$this->isNewRecord) {
            $this->selected_branches = ArrayHelper::map($this->getBranchIds(), 'branch_id', 'branch_id');

        }

        return parent::loadDefaultValues($skipIfSet);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $this->msisdn_warning = str_replace(' ', '', $this->msisdn_warning);
        if(!$this->isNewRecord) {
            $currentBranchIds = $this->getBranchIds();

            if (count($currentBranchIds)) {
                foreach ($currentBranchIds as $index => $branch) {

                    // Neu truoc do da chon --> xoa bot di
                    $exists = false;
                    if (is_array($this->selected_branches) && count($this->selected_branches)) {
                        foreach ($this->selected_branches as $selectedId) {
                            if($selectedId == $branch->branch_id) {
                                $exists = true;
                                break;
                            }
                        }

                        // neu bo chon --> xoa di
                        if($exists === false) {
                            BranchPartner::deleteAll([
                                'partner_id' => $this->id,
                                'branch_id' => $branch->branch_id,
                            ]);

                        }
                    } else {

                        BranchPartner::deleteAll([
                            'partner_id' => $this->id,
                        ]);
                    }

                }
            }

            // them nhung partner moi
            if (is_array($this->selected_branches) && count($this->selected_branches)) {
                foreach ($this->selected_branches as $item) {
                    // tag co san
                    $exists = false;

                    foreach ($currentBranchIds as $branch) {
                        if($branch->branch_id == $item) {
                            $exists = true;
                            break;
                        }
                    }

                    // neu post chua co tag nay thi gan tag nay vao post
                    if($exists === false) {
                        $tag = new BranchPartner();
                        $tag->partner_id = $this->id;
                        $tag->branch_id = $item;
                        $tag->save();
                    }

                }
            }

            return parent::save($runValidation, $attributeNames);

        } else {
            $return = parent::save($runValidation, $attributeNames);

            if (is_array($this->selected_branches) && count($this->selected_branches)) {
                foreach ($this->selected_branches as $item) {
                    // tag co san
                    if ($item) {
                        $tag = new BranchPartner();
                        $tag->partner_id = $this->id;
                        $tag->branch_id = $item;
                        $tag->save();
                    }
                }
            }

            return $return;
        }


    }

    public function getBranchIds()
    {
        return BranchPartner::find()
            ->select('branch_id')
            ->where(
                ['partner_id' => $this->id]
            )
            ->all();
    }
    public function getBranches() {
        return $this->hasMany(Branch::className(), ['id' => 'branch_id'])
            ->viaTable('branch_partner', ['partner_id' => 'id'])
            ->orderBy('name asc')
            ;
    }

    /**
     * Tra ve mang dang [id => name]
     * Dung trong select box
     * @return array
     */
    public static function getPartnerArray() {
        $itemArr = self::find()
            ->select('id, name')
            ->asArray()
            ->orderBy('name asc')
            ->all();

        if (!empty($itemArr)) {
            return  \yii\helpers\ArrayHelper::map($itemArr, 'id', 'name');

        } else {
            return array();
        }
    }

    public static function getPartnersByBranch($branchId) {
        $branch = Branch::findOne($branchId);

        if ($branch) {
            return $branch->partners;
        }
        return null;
    }

    public static function getPartnersArrByBranch($branchId) {
        $data = [];
        $branch = Branch::findOne($branchId);
        if ($branch) {
            $data = $branch->getPartners()
                ->select('id, name')
                ->orderBy('name ASC')
                ->asArray()
                ->all();
        }

        if (count($data)) {
            $data = array_values($data);
        }

        return $data;
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'name_title' => Yii::t('backend', 'Gender'),
            'ruc' => Yii::t('backend', 'Ruc'),
            'director' => Yii::t('backend', 'Director'),
            'director_dni' => Yii::t('backend', 'Director Dni'),
            'address' => Yii::t('backend', 'Address'),
            'description' => Yii::t('backend', 'Description'),
            'status' => Yii::t('backend', 'Status'),
            'msisdn' => Yii::t('backend', 'Director phone number'),
            'msisdn_warning' => Yii::t('backend', 'Warning phone numbers'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

}