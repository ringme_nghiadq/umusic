<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MscCollection;

/**
 * MscCollectionSearch represents the model behind the search form about `backend\models\MscCollection`.
 */
class MscCollectionSearch extends MscCollection
{
    public function formName()
    {
        return '';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active', 'number_of_artists'], 'integer'],
            [['id', 'collection_name', 'description', 'note', 'updated_at', 'created_at'], 'safe'],
            [['collection_name', 'description', 'note', 'updated_at', 'created_at'], 'trim'],
            [['collection_name', 'description', 'note', 'updated_at', 'created_at'], 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MscCollection::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'collection_name', $this->collection_name])

        ;

        if ($this->updated_at != Yii::t('backend', 'All') && strpos($this->updated_at, ' - ') > 0) {
            $request_times = \common\helpers\Helpers::splitDate($this->updated_at, 'd/m/Y');
            $query->andFilterWhere(['BETWEEN', 'updated_at', $request_times[0], $request_times[1]]);
        }

        if ($this->is_active != "". MscCollection::STATUS_DELETE) {
            $query->andFilterWhere(['!=', 'is_active' , -1]);
        }
        $query->andFilterWhere([
            'is_active' => $this->is_active,
        ]);

        return $dataProvider;
    }
}
