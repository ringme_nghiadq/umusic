<?php

use yii\widgets\Pjax;


?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 class="modal-title"><?= Yii::t('backend', 'File manager') ?></h4>
</div>
<div class="modal-body">
    <?= $this->render('child', [
        'folders' => $folders,
        'files' => $files,
        'parent' => $parent,
    ]); ?>

</div>
<style type="text/css">
    .folder {
        display: inline-block !important;
        padding: 5px;
        width: 40%;
        cursor: pointer;
    }
    .file {cursor: pointer}
    .file:hover {color: blue}
    .bfolder {cursor: pointer}
    li.active .bfolder {cursor: default}
    #ftp-upload-folder .breadcrumb > li + li:before {
        padding: 0;
    }
    #ftp-upload-folder .breadcrumb {
        padding-left: 5px !important;
    }
</style>