<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Song;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\MscCollectionContentSearch */
/* @var $form yii\widgets\ActiveForm */
$song = Song::findOne($model->contentId);

?>
<div class="panel panel-default msc-collection-content-search">
    <div class="panel-body row">
        <?php $form = ActiveForm::begin([
            'action' => [$page],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'content')->textInput() ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'contentId')->widget(Select2::classname(), [
                'initValueText' => $song ? $song->song_name : '',
                'options' => ['placeholder' => Yii::t('backend', 'Find song')],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return '" . Yii::t('backend', 'Loading') . " ...'; }"),
                        'inputTooShort' => new JsExpression("function () { return '" . Yii::t('backend', 'Input at least 3 characters') . " ...'; }"),
                        'inputTooLong' => new JsExpression("function () { return '" . Yii::t('backend', 'Input maximum 255 characters') . "'; }"),
                        'noResults' => new JsExpression("function () { return '" . Yii::t('backend', 'No result found') . "'; }"),
                        'searching' => new JsExpression("function () { return '" . Yii::t('backend', 'Searching') . " ...'; }"),
                    ],
                    'ajax' => [
                        'url' => \yii\helpers\Url::toRoute(['song/ajax-search']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term }; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(result) { return result.text; }'),
                    'templateSelection' => new JsExpression('function (result) { return result.text; }')
                ]
            ]); ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'commentAt')->widget(\kartik\daterange\DateRangePicker::className(), [
                'options' => ['class' => 'form-control daterange-field input-sm'],
                'model' => $model,
                'attribute' => 'commentAt',
                'convertFormat' => true,
                'presetDropdown' => true,
                'readonly' => true,
                'pluginOptions' => [
                    'opens' => 'left',
                    'alwaysShowCalendars' => true,
                    'timePickerIncrement' => 30,
                    'locale' => [
                        'format' => 'd/m/Y',
                    ]
                ],
                'pluginEvents'=> [
                    'cancel.daterangepicker' => "function(ev, picker) {
                        $(this).val('');
                    }",
                    'apply.daterangepicker' => 'function(ev, picker) {
                        if($(this).val() == "") {
                            $(this).val(picker.startDate.format(picker.locale.format) + picker.locale.separator +
                            picker.endDate.format(picker.locale.format)).trigger("change");
                        }
                    }',
                    'show.daterangepicker' => 'function(ev, picker) {
                        picker.container.find(".ranges").off("mouseenter.daterangepicker", "li");
                        if($(this).val() == "") {
                            picker.container.find(".ranges .active").removeClass("active");
                        }
                    }',
                ]
            ]) ?>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Reset',[$page], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
