<?php


use \yii\grid\GridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;
use backend\models\Album;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AlbumCommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$searchPartialNames = [
    'album-comment' => '_albumSearch',
    'song-comment' => '_songSearch',
    'artist-comment' => '_artistSearch',
    'playlist-comment' => '_playlistSearch',
    'author-comment' => '_authorSearch',
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row comment-album">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php echo $this->render($searchPartialNames[$page], ['model' => $searchModel, 'page' => $page]); ?>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <?php 
                    //Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                    ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterSelector' => 'select[name="per-page"]',
                        'layout' => "{items}\n <div class='form-inline pagination page-size'>". awesome\backend\grid\AwsPageSize::widget([
                            'options' => [
                            'class' => 'form-control  form-control-sm',
                            ]]). '</div> <div class="col-md-6">{pager}</div> <div class="pagination col-md-3 text-right total-count">'. Yii::t('backend', 'Tổng số'). ': <b>'. number_format($dataProvider->getTotalCount()). '</b> '.Yii::t('backend', 'bản ghi').'</div>',
                        'columns' => [
                            [
                                'attribute' => 'contentId',
                                'value' => function($model) {
                                    return $model->getContentName();
                                },
                                'format' => 'html',
                            ],
                            [
                                'attribute' => 'content',
                            ],
                            'name',
                            [
                                'attribute' => 'commentAt',
                                'value' => function($model) {
                                    return $model->commentAt->toDateTime()->format('d/m/Y H:i:s');
                                },
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'headerOptions' => ['class' => 'head-actions'],
                                'contentOptions' => ['class' => 'row-actions'],
                                'template' => '{delete}',
                            ],
                        ],
                    ]); ?>
                    
                    <?php 
                    //Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>