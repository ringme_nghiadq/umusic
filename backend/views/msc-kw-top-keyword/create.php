<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MscKwTopKeyword */

$this->title = 'Create Top Keyword';
$this->params['breadcrumbs'][] = ['label' => 'Top Keywords', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row msc-kw-top-keyword-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
