<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $model backend\models\MscKwTopKeyword */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php  $form = ActiveForm::begin(); ?>

    <div class="portlet light portlet-fit portlet-form bordered msc-kw-top-keyword-form">

        <div class="portlet-body">
            <div class="form-body row">
                    <div class="col-md-6">
        <?= $form->field($model, 'keyword')->textInput(['maxlength' => 100]) ?>

    </div>

            </div>
        </div>
        <div class="portlet-title ">
            


                <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
                    <i class="fa fa-angle-left"></i> <?=  'Back' ?>                </a>
                &nbsp;&nbsp;&nbsp;
                <?=  Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-transparent green  btn-sm']) ?>


        </div>
    </div>

<?php ActiveForm::end(); ?>
