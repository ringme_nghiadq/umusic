<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MscKwTopKeyword */

$this->title = 'Update Top Keyword: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Top Keywords', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row msc-kw-top-keyword-update">
    <div class="col-md-12">

    <?= $this->render('_form', [
        'model' => $model,
        'title' => $this->title
    ]) ?>

    </div>
</div>
