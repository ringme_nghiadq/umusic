<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MscCountrySearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default msc-country-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'country_code') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'country_name') ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'country_name_en') ?>

        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('backend', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
