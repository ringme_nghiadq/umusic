<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MscCountry */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Msc Country',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Msc Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row msc-country-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
