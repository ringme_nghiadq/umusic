<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MscCountry */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Msc Country',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Msc Countries'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update') . ' ' . $model->id;
?>
<div class="row msc-country-update">
    <div class="col-md-12">

    <?= $this->render('_form', [
        'model' => $model,
        'title' => $this->title
    ]) ?>

    </div>
</div>
