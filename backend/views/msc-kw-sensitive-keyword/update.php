<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MscKwSensitiveKeyword */

$this->title = 'Update Sensitive Keyword: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sensitive Keywords', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row msc-kw-sensitive-keyword-update">
    <div class="col-md-12">

    <?= $this->render('_form', [
        'model' => $model,
        'title' => $this->title
    ]) ?>

    </div>
</div>
