<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MscKwSensitiveKeyword */

$this->title = 'Create Sensitive Keyword';
$this->params['breadcrumbs'][] = ['label' => 'Sensitive Keywords', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row msc-kw-sensitive-keyword-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
