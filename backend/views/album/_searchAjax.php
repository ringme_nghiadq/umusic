<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\AlbumSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default album-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['/album/list-ajax', 'ctype' => $ctype, 'ctype_id' => $ctypeId],
            'method' => 'get',
            'id' => 'album-search-ajax',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'country_code')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\MscCountry::find()->all(), 'country_code', 'country_name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'All'),
                    'id' => 'country-id'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'album_name') ?>

        </div>



        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('backend', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
