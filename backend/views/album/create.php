<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Album */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Album',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Albums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row album-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
