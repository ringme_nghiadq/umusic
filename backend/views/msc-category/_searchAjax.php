<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MscCategorySearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default category-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['/msc-category/list-ajax', 'ctype' => $ctype, 'ctype_id' => $ctypeId],
            'method' => 'get',
            'id' => 'category-search-ajax',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'cate_name') ?>

        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('backend', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
