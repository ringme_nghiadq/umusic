<?php

use \yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = $model->cate_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Category'), 'url' => ['msc-category/index']];
$this->params['breadcrumbs'][] = $model->cate_name;
?>
<div class="row playlist-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <h3><?= Yii::t('backend', 'Category') ?>: <?= $model->cate_name ?></h3>
                <br/>
                <div class="">
                    <?= Html::button(Yii::t('backend', 'Add playlist', [
                    ]),
                        [
                            'class' => 'btn btn-info btn-sm',
                            'data-target' => '#search-playlist-modal',
                            'data-toggle' => "modal"
                        ]) ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <?php
                    Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                    ?>

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>
                                <?= Yii::t('backend', 'Playlist name'); ?>
                            </th>
                            <th>
                                <?= Yii::t('backend', 'Song num'); ?>
                            </th>
                            <th>
                                <?= Yii::t('backend', 'Created At'); ?>
                            </th>
                            <th>
                                <?= Yii::t('backend', 'Actions'); ?>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (count($playlists)): ?>
                            <?php foreach ($playlists as $index => $content): ?>
                                <tr id="playlist-<?= $content->id; ?>">

                                    <td><?= $content->playlist_name ?></td>
                                    <td>
                                        <?= number_format($content->number_of_songs); ?>
                                    </td>
                                    <td>
                                        <?= $content->created_at; ?>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-danger btn-playlist-remove"
                                                ahref="<?= \yii\helpers\Url::to([
                                                    'playlist/remove-to-category',
                                                    'ctype' => 'category',
                                                    'ctype_id' => $model->id,
                                                    'playlist_id' => $content->id,
                                                ]); ?>" playlist_id="<?= $content->id ?>"><?= Yii::t('backend', 'Remove') ?></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </tbody>
                    </table>


                    <?php
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="detail-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

        </div>

    </div>
</div>

<!-- Modal -->
<div id="search-playlist-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <?php echo $this->render('/playlist/_searchAjax', [
                'model' => $searchModel,
                'ctype' => 'category',
                'ctypeId' => $model->id,
            ]); ?>
            <?php
            Pjax::begin(['timeout' => 0, 'formSelector' => 'form#playlist-search-ajax', 'enablePushState' => false, 'id' => 'playlistGridPjax']);
            ?>

            <?php Pjax::end(); ?>

            <div class="clearfix"></div>
        </div>

    </div>
</div>
<?php
$this->registerJs(<<< EOT_JS_CODE
    $(document).ready(function() {
        $('#search-playlist-modal').on('click', '.btn-playlist-add', function() {
            var url = $(this).attr('ahref');
            var playlistId = $(this).attr('playlist_id');

            $.get(url, function(resp) {

                if (resp.error_code == 0) {
                    $.pjax.reload({container:"#mainGridPjax",timeout:0})
                    toastr.info(resp.message);
                } else {

                }
            });
        });

        $('#mainGridPjax').on('click', '.btn-playlist-remove', function() {
            var url = $(this).attr('ahref');
            var playlistId = $(this).attr('playlist_id');

            $.get(url, function(resp) {

                if (resp.error_code == 0) {
                    $.pjax.reload({container:"#mainGridPjax",timeout:0})
                    toastr.info(resp.message);
                } else {

                }
            });
        })
    });

EOT_JS_CODE
);
?>
