<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MscCategory */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Msc Category',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Msc Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row msc-category-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
