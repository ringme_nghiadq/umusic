<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\select2\Select2;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\MscCategory */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered msc-category-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'cate_name')->textInput(['maxlength' => 255]) ?>

            </div>
            <div class="col-md-6">
                <?php echo $form->field($model, 'selected_countries', [
                ])->widget(Select2::classname(), [

                    'data' => \backend\models\MscCountry::getCountryArray(),
                    //'maintainOrder' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'language' => Yii::$app->language,
                    'options' => [
                        'none-text' => Yii::t('backend', 'Choose countries'),
                        'placeholder' => Yii::t('backend', 'Choose countries'),
                        'multiple' => true,
                    ],
                ]); ?>
            </div>

            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>
                <br />
                <?= $form->field($model, 'is_active')->checkbox() ?>

            </div>
            <div class="col-md-6">

                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'avatar',
                    'itemName' => 'cate-avatar', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Avatar'),
                    'dataMinSize' => '100,100',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '100:100',
                    'model' => $model,
                    'dataWillRemove' => 'avatarWillChange',
                    'dataWillSave' => 'avatarWillChange',
                    'helpBlock' => Yii::t('backend', 'Ratio 100 x 100px(.jpg|png|jpeg)'),
                    'accept' => 'image/jpeg,image/jpg,image/png',
                ]) ?>

            </div>
            <div class="col-md-6">
                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'banner',
                    'itemName' => 'cate-banner', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Banner'),
                    'dataMinSize' => '1080,368',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '1080:368',
                    'model' => $model,
                    'dataWillRemove' => 'headerWillChange',
                    'dataWillSave' => 'headerWillChange',
                    'helpBlock' => Yii::t('backend', 'Ratio 1080 x 368px (.jpg|png|jpeg)'),
                    'accept' => 'image/jpeg,image/jpg,image/png',
                ]) ?>
            </div>



        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>

<?php ActiveForm::end(); ?>
<?php \common\components\slim\SlimAsset::register($this); ?>

