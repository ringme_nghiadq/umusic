<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MscShowOn */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Show On',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Show Ons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row msc-show-on-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
