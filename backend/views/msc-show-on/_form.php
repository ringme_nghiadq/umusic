¬<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $model backend\models\MscShowOn */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered msc-show-on-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'position_key')->textInput(['maxlength' => 100]) ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'position_name')->textInput(['maxlength' => 100]) ?>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'content_id')->textInput() ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'content_type')->textInput(['maxlength' => 20]) ?>

            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'note')->textInput(['maxlength' => 250]) ?>

            </div>

        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>

<?php ActiveForm::end(); ?>
