<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Playlist */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Playlist',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Playlists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row playlist-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
