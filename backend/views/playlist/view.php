<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Playlist */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Playlists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row playlist-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <?php if (!$isAjax): ?>
                <div class="portlet-title">

                    <div class="">
                        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id],
                            ['class' => 'btn btn-info  btn-sm'])
                        ?>
                        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-transparent red  btn-sm',
                            'data' => [
                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'playlist_name',
                        [
                            'attribute' => 'avatar',
                            'value' => function ($model) {
                                return $model->getAvatarUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:120px'],
                        ],
                        [
                            'attribute' => 'banner',
                            'value' => function ($model) {
                                return $model->getBannerUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:120px'],
                        ],

                        'description:ntext',
                        [
                            'attribute' => 'is_active',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->is_active == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],
                        'number_of_songs:integer',
                        'number_of_artists:integer',
                        'slug',
                        'total_listens:integer',
                        'total_download:integer',
                        'total_liked:integer',
                        'total_shared:integer',
                        'total_comment:integer',
                        //'boost_score',

                        //'belong',
                        //'userId',
                        'updated_at',
                        'created_at',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
