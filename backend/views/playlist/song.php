<?php

use \yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = $model->playlist_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', ucfirst($ctype)), 'url' => [$ctype. '/index']];
$this->params['breadcrumbs'][] = $model->playlist_name;
?>
    <div class="row song-index">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <h3><?= Yii::t('backend', 'Playlist') ?>: <?= $model->playlist_name ?></h3>
                    <br/>
                    <div class="">
                        <?= Html::button(Yii::t('backend', 'Add song', [
                        ]),
                            [
                                'class' => 'btn btn-info btn-sm',
                                'data-target' => '#search-song-modal',
                                'data-toggle' => "modal"
                            ]) ?>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <?php
                        Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                        ?>

                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th><?= Yii::t('backend', 'Country'); ?></th>
                                <th>
                                    <?= Yii::t('backend', 'Song name'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Author'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Published time'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Actions'); ?>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (count($songs)): ?>
                                <?php foreach ($songs as $index => $song): ?>
                                    <tr id="song-<?= $song->id; ?>">
                                        <td>
                                            <?= ($song->country) ? $song->country->country_name : null; ?>
                                        </td>
                                        <td><?= $song->song_name ?></td>
                                        <td>
                                            <?= $song->author ? $song->author->alias_name : null; ?>
                                        </td>
                                        <td>
                                            <?= $song->published_time; ?>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-danger btn-song-remove"
                                                    ahref="<?= \yii\helpers\Url::to([
                                                        'song/remove-to-collection',
                                                        'ctype' => $ctype,
                                                        'ctype_id' => $model->id,
                                                        'song_id' => $song->id,
                                                    ]); ?>"
                                                    song_id="<?= $song->id ?>"><?= Yii::t('backend', 'Remove') ?></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>

                        <?php
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="detail-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="search-song-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <?php echo $this->render('/song/_searchAjax', [
                    'model' => $searchModel,
                    'ctype' => $ctype,
                    'ctypeId' => $model->id,
                ]); ?>
                <?php
                Pjax::begin(['timeout' => 0, 'formSelector' => 'form#song-search-ajax', 'enablePushState' => false, 'id' => 'songGridPjax']);
                ?>

                <?php Pjax::end(); ?>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
<?php
$this->registerJs(<<< EOT_JS_CODE
    $(document).ready(function() {
        $('#search-song-modal').on('click', '.btn-song-add', function() {
            var url = $(this).attr('ahref');
            var songId = $(this).attr('song_id');

            $.get(url, function(resp) {

                if (resp.error_code == 0) {
                    $.pjax.reload({container:"#mainGridPjax",timeout:0})
                    toastr.info(resp.message);
                } else {

                }
            });
        });

        $('#mainGridPjax').on('click', '.btn-song-remove', function() {
            var url = $(this).attr('ahref');
            var songId = $(this).attr('song_id');

            $.get(url, function(resp) {

                if (resp.error_code == 0) {
                    $.pjax.reload({container:"#mainGridPjax",timeout:0})
                    toastr.info(resp.message);
                } else {

                }
            });
        })
    });

EOT_JS_CODE
);
?>