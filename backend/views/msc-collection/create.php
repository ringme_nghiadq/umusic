<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MscCollection */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Msc Collection',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Msc Collections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row msc-collection-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
