<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MscCollectionSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default msc-collection-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'collection_name') ?>

        </div>

        <div class="col-md-3">
            <?php echo
            $form->field($model, 'is_active')->dropDownList(
                \common\helpers\Helpers::commonStatusArr(),
                ['prompt' => Yii::t('backend', 'All')]
            );;

            ?>

        </div>


        <div class="col-md-3">
            <?php echo

            $form->field($model, 'created_at')->widget(\kartik\daterange\DateRangePicker::className(), [
                    'options' => ['class' => 'form-control daterange-field input-sm'],
                    'model' => $model,
                    'attribute' => 'created_at',
                    'convertFormat' => true,
                    'presetDropdown' => true,
                    'readonly' => true,
                    'pluginOptions' => [
                        'opens' => 'left',
                        'alwaysShowCalendars' => true,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'd/m/Y',
                        ]
                    ],
                    'pluginEvents' => [
                        'cancel.daterangepicker' => "function(ev, picker) {
                                            $(this).val('');
                                        }",
                        'apply.daterangepicker' => 'function(ev, picker) {
                                            if($(this).val() == "") {
                                                $(this).val(picker.startDate.format(picker.locale.format) + picker.locale.separator +
                                                picker.endDate.format(picker.locale.format)).trigger("change");
                                            }
                                        }',
                        'show.daterangepicker' => 'function(ev, picker) {
                                            picker.container.find(".ranges").off("mouseenter.daterangepicker", "li");
                                            if($(this).val() == "") {
                                                picker.container.find(".ranges .active").removeClass("active");
                                            }
                                        }',
                    ]
                ]
            )
            ?>

        </div>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('backend', 'Reset'), ['index'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
