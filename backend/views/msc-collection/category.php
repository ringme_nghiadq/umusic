<?php

use \yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = $model->collection_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Collection'), 'url' => ['msc-collection/index']];
$this->params['breadcrumbs'][] = $model->collection_name;
?>
    <div class="row category-index">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <h3><?= Yii::t('backend', 'Collection') ?>: <?= $model->collection_name ?></h3>
                    <br/>
                    <div class="">
                        <?= Html::button(Yii::t('backend', 'Add category', [
                        ]),
                            [
                                'class' => 'btn btn-info btn-sm',
                                'data-target' => '#search-category-modal',
                                'data-toggle' => "modal"
                            ]) ?>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <?php
                        Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                        ?>

                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>
                                    <?= Yii::t('backend', 'Category name'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Description'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Created At'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Actions'); ?>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (count($list)): ?>
                                <?php foreach ($list as $index => $content): ?>
                                    <tr id="category-<?= $content->id; ?>">

                                        <td><?= $content->cate_name ?></td>
                                        <td>
                                            <?= ($content->description); ?>
                                        </td>
                                        <td>
                                            <?= $content->created_at; ?>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-danger btn-category-remove"
                                                    ahref="<?= \yii\helpers\Url::to([
                                                        'msc-category/remove-to-collection',
                                                        'ctype' => 'collection',
                                                        'ctype_id' => $model->id,
                                                        'category_id' => $content->id,
                                            ]); ?>" category_id="<?= $content->id ?>"><?= Yii::t('backend', 'Remove') ?></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>


                        <?php
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="detail-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="search-category-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <?php echo $this->render('/msc-category/_searchAjax', [
                    'model' => $searchModel,
                    'ctype' => 'collection',
                    'ctypeId' => $model->id,
                ]); ?>
                <?php
                Pjax::begin(['timeout' => 0, 'formSelector' => 'form#category-search-ajax', 'enablePushState' => false, 'id' => 'categoryGridPjax']);
                ?>

                <?php Pjax::end(); ?>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
<?php
$this->registerJs(<<< EOT_JS_CODE
    $(document).ready(function() {
        $('#search-category-modal').on('click', '.btn-category-add', function() {
            var url = $(this).attr('ahref');
            var categoryId = $(this).attr('category_id');

            $.get(url, function(resp) {

                if (resp.error_code == 0) {
                    $.pjax.reload({container:"#mainGridPjax",timeout:0})
                    toastr.info(resp.message);
                } else {

                }
            });
        });

        $('#mainGridPjax').on('click', '.btn-category-remove', function() {
            var url = $(this).attr('ahref');
            var categoryId = $(this).attr('category_id');

            $.get(url, function(resp) {

                if (resp.error_code == 0) {
                    $.pjax.reload({container:"#mainGridPjax",timeout:0})
                    toastr.info(resp.message);
                } else {

                }
            });
        })
    });

EOT_JS_CODE
);
?>