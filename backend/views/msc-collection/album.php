<?php

use \yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = $model->collection_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Collection'), 'url' => ['msc-collection/index']];
$this->params['breadcrumbs'][] = $model->collection_name;
?>
    <div class="row album-index">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <h3><?= Yii::t('backend', 'Collection') ?>: <?= $model->collection_name ?></h3>
                    <br/>
                    <div class="">
                        <?= Html::button(Yii::t('backend', 'Add album', [
                        ]),
                            [
                                'class' => 'btn btn-info btn-sm',
                                'data-target' => '#search-album-modal',
                                'data-toggle' => "modal"
                            ]) ?>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <?php
                        Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                        ?>

                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th><?= Yii::t('backend', 'Country'); ?></th>
                                <th>
                                    <?= Yii::t('backend', 'Album name'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Song num'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Published time'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Actions'); ?>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (count($albums)): ?>
                                <?php foreach ($albums as $index => $content): ?>
                                    <tr id="album-<?= $content->id; ?>">
                                        <td>
                                            <?= ($content->country)? $content->country->country_name: null; ?>
                                        </td>
                                        <td><?= $content->album_name ?></td>
                                        <td>
                                            <?= number_format($content->number_of_songs); ?>
                                        </td>
                                        <td>
                                            <?= $content->published_time; ?>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-danger btn-album-remove"
                                                    ahref="<?= \yii\helpers\Url::to([
                                                        'album/remove-to-collection',
                                                        'ctype' => 'collection',
                                                        'ctype_id' => $model->id,
                                                        'album_id' => $content->id,
                                            ]); ?>" album_id="<?= $content->id ?>"><?= Yii::t('backend', 'Remove') ?></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>


                        <?php
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="detail-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="search-album-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <?php echo $this->render('/album/_searchAjax', [
                    'model' => $searchModel,
                    'ctype' => 'collection',
                    'ctypeId' => $model->id,
                ]); ?>
                <?php
                Pjax::begin(['timeout' => 0, 'formSelector' => 'form#album-search-ajax', 'enablePushState' => false, 'id' => 'albumGridPjax']);
                ?>

                <?php Pjax::end(); ?>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
<?php
$this->registerJs(<<< EOT_JS_CODE
    $(document).ready(function() {
        $('#search-album-modal').on('click', '.btn-album-add', function() {
            var url = $(this).attr('ahref');
            var albumId = $(this).attr('album_id');

            $.get(url, function(resp) {

                if (resp.error_code == 0) {
                    $.pjax.reload({container:"#mainGridPjax",timeout:0})
                    toastr.info(resp.message);
                } else {

                }
            });
        });

        $('#mainGridPjax').on('click', '.btn-album-remove', function() {
            var url = $(this).attr('ahref');
            var albumId = $(this).attr('album_id');

            $.get(url, function(resp) {

                if (resp.error_code == 0) {
                    $.pjax.reload({container:"#mainGridPjax",timeout:0})
                    toastr.info(resp.message);
                } else {

                }
            });
        })
    });

EOT_JS_CODE
);
?>