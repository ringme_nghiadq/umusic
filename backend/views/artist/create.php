<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Artist */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Artist',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Artists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row artist-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
