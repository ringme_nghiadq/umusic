<?php

use \yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = $model->alias_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'artist'), 'url' => ['artist/index']];
$this->params['breadcrumbs'][] = $model->alias_name;
?>
    <div class="row song-index">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">
                    <h3><?= Yii::t('backend', 'artist') ?>: <?= $model->alias_name ?></h3>
                    <br/>
                    <div class="">
                        <?= Html::button(Yii::t('backend', 'Add song', [
                        ]),
                            [
                                'class' => 'btn btn-info btn-sm',
                                'data-target' => '#search-song-modal',
                                'data-toggle' => "modal"
                            ]) ?>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-container">
                        <?php
                        Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                        ?>

                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th><?= Yii::t('backend', 'Country'); ?></th>
                                <th>
                                    <?= Yii::t('backend', 'Song name'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Author'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Published time'); ?>
                                </th>
                                <th>
                                    <?= Yii::t('backend', 'Actions'); ?>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (count($songs)): ?>
                                <?php foreach ($songs as $index => $song): ?>
                                    <tr id="song-<?= $song->id; ?>">
                                        <td>
                                            <?= ($song->country)? $song->country->country_name: null; ?>
                                        </td>
                                        <td><?= $song->song_name ?></td>
                                        <td>
                                            <?= ($song->author)? $song->author->alias_name: null; ?>
                                        </td>
                                        <td>
                                            <?= $song->published_time; ?>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-danger btn-song-remove"
                                                    ahref="<?= \yii\helpers\Url::to([
                                                        'song/remove-to-collection',
                                                        'ctype' => 'artist',
                                                        'ctype_id' => $model->id,
                                                        'song_id' => $song->id,
                                                    ]); ?>" song_id="<?= $song->id ?>"><?= Yii::t('backend', 'Remove') ?></button>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                        <?php /*  GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterSelector' => 'select[name="per-page"]',
                            'layout' => "{items}\n <div class='form-inline pagination page-size'>" . awesome\backend\grid\AwsPageSize::widget([
                                    'options' => [
                                        'class' => 'form-control  form-control-sm',
                                    ]]) . '</div> <div class="col-md-6">{pager}</div> <div class="pagination col-md-3 text-right total-count">' . Yii::t('backend', 'Tổng số') . ': <b>' . number_format($dataProvider->getTotalCount()) . '</b> ' . Yii::t('backend', 'bản ghi') . '</div>',
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                //['class' => 'yii\grid\SerialColumn'],
                                [
                                    'attribute' => 'avatar',
                                    'value' => function ($model) {
                                        return $model->getAvatarUrl();
                                    },
                                    'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                                    'filter' => false,
                                    'headerOptions' => ['style' => 'width:120px'],
                                ],
                                [
                                    'attribute' => 'country_code',
                                    'format' => 'raw',
                                    'value' => function ($object) {
                                        return $object->country ? $object->country->country_name : null;
                                    }
                                ],
                                'song_name',
                                [
                                    'attribute' => 'author_id',
                                    'format' => 'raw',
                                    'value' => function ($object) {
                                        return $object->author ? $object->author->alias_name : null;
                                    }
                                ],
                                [
                                    'attribute' => 'is_active',
                                    'format' => 'raw',
                                    'value' => function ($object) {
                                        $class = ($object->is_active == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                        return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                                    }
                                ],
                                [
                                    'attribute' => 'is_download',
                                    'format' => 'raw',
                                    'value' => function ($object) {
                                        $class = ($object->is_download == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                        return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                                    }
                                ],
                                //'avatar',
                                //'banner',
                                //'thumbnail',
                                // 'media_path',
                                // 'lyric:ntext',

                                // 'bit_rate',
                                // 'duration',
                                // 'slug',
                                // 'is_download',
//                             'total_listens:integer',
//                             'total_download:integer',
//                             'total_liked:integer',
//                             'total_shared:integer',
//                             'total_comment:integer',
                                'total_score:integer',
                                // 'number_of_artists',
                                'published_time',
                                'created_at',
                                'updated_at',

                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'headerOptions' => ['style' => 'width: 50px;', 'class' => 'head-actions'],
                                    'contentOptions' => ['class' => 'row-actions'],
                                    'template' => '{view}',
                                    'buttons' => [
                                        'view' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['song/view', 'id' => $model->id], [
                                                'class' => '',
                                                'data-target' => '#detail-modal',
                                                'data-toggle' => "modal"
                                            ]);
                                        },
                                    ]
                                ],
                            ],
                        ]); */  ?>

                        <?php
                        Pjax::end();
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="detail-modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">

            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="search-song-modal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <?php echo $this->render('/song/_searchAjax', [
                    'model' => $searchModel,
                    'ctype' => 'artist',
                    'ctypeId' => $model->id,
                ]); ?>
                <?php
                Pjax::begin(['timeout' => 0, 'formSelector' => 'form#song-search-ajax', 'enablePushState' => false, 'id' => 'songGridPjax']);
                ?>

                <?php Pjax::end(); ?>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
<?php
$this->registerJs(<<< EOT_JS_CODE
    $(document).ready(function() {
        $('#search-song-modal').on('click', '.btn-song-add', function() {
            var url = $(this).attr('ahref');
            var songId = $(this).attr('song_id');

            $.get(url, function(resp) {

                if (resp.error_code == 0) {
                    $.pjax.reload({container:"#mainGridPjax",timeout:0})
                    toastr.info(resp.message);
                } else {

                }
            });
        });

        $('#mainGridPjax').on('click', '.btn-song-remove', function() {
            var url = $(this).attr('ahref');
            var songId = $(this).attr('song_id');

            $.get(url, function(resp) {

                if (resp.error_code == 0) {
                    $.pjax.reload({container:"#mainGridPjax",timeout:0})
                    toastr.info(resp.message);
                } else {

                }
            });
        })
    });

EOT_JS_CODE
);
?>