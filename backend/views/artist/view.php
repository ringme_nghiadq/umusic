<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Artist */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Artists'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row artist-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <?php if (!$isAjax): ?>
                <div class="portlet-title">

                    <div class="">
                        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id],
                            ['class' => 'btn btn-info  btn-sm'])
                        ?>
                        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-transparent red  btn-sm',
                            'data' => [
                                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                        ])
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute' => 'country_code',
                            'format' => 'raw',
                            'value' => function ($object) {
                                return $object->country ? $object->country->country_name : null;
                            }
                        ],
                        'alias_name',
                        'real_name',

                        'boost_score',
                        [
                            'attribute' => 'avatar',
                            'value' => function ($model) {
                                return $model->getAvatarUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:120px'],
                        ],
                        [
                            'attribute' => 'banner',
                            'value' => function ($model) {
                                return $model->getBannerUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:120px'],
                        ],

                        'info:ntext',
                        'birthday',
                        'gender',
                        'address',
                        [
                            'attribute' => 'is_active',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->is_active == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],
                        'slug',
                        'updated_at',
                        'created_at',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
