<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\Artist */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered artist-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'country_code')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\MscCountry::find()->all(), 'country_code', 'country_name'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose a country'),
                        'id' => 'country-id'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>


                <?= $form->field($model, 'alias_name')->textInput(['maxlength' => 255]) ?>

                <?= $form->field($model, 'real_name')->textInput(['maxlength' => 255]) ?>
                <br/>
                <?= $form->field($model, 'is_active')->checkbox() ?>

                <?=
                $form->field($model, 'birthday', [
                    'template' => '{label}{input}{error}{hint}',
                ])->widget(\kartik\widgets\DatePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
                ?>

                <?= $form->field($model, 'gender')->radioList(\common\models\ArtistBase::getAllGenders()) ?>

                <?= $form->field($model, 'address')->textarea(['rows' => 4]) ?>
            </div>
            <div class="col-md-6">
                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'avatar',
                    'itemName' => 'artist-avatar', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Avatar'),
                    'dataMinSize' => '100,100',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '100:100',
                    'model' => $model,
                    'dataWillRemove' => 'avatarWillChange',
                    'dataWillSave' => 'avatarWillChange',
                    'helpBlock' => Yii::t('backend', 'Ratio 100 x 100px(.jpg|png|jpeg)'),
                    'accept' => 'image/jpeg,image/jpg,image/png',
                ]) ?>

                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'banner',
                    'itemName' => 'artist-banner', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Banner'),
                    'dataMinSize' => '1080,368',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '1080:368',
                    'model' => $model,
                    'dataWillRemove' => 'headerWillChange',
                    'dataWillSave' => 'headerWillChange',
                    'helpBlock' => Yii::t('backend', 'Ratio 1080 x 368px (.jpg|png|jpeg)'),
                    'accept' => 'image/jpeg,image/jpg,image/png',
                ]) ?>

                <?= $form->field($model, 'info')->textarea(['rows' => 4]) ?>
            </div>


            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'boost_score')->textInput() ?>

            </div>

        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>

<?php ActiveForm::end(); ?>
<?php \common\components\slim\SlimAsset::register($this); ?>
