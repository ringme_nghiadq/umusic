<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\MscCollection;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\MscCollectionContentSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default msc-collection-content-search">
    <div class="panel-body row">
        <?php $form = ActiveForm::begin([
            'action' => [$page],
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'collection_id')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(MscCollection::find()
                    ->where(['id' => $collectionIds ])
                    ->all(), 'id', 'collection_name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'All'),
                    'id' => 'cate-id'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); ?>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Reset',[$page], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
