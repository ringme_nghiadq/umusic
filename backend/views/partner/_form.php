<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model backend\models\Partner */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered partner-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'ruc')->textInput(['maxlength' => 200]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => 200]) ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'name_title')->dropDownList([
                    'Mr' => 'Mr',
                    'Mrs' => 'Mrs',
                    'Ms' => 'Ms',
                    'Miss' => 'Miss',
                ], [
                    'prompt' => Yii::t('backend', 'Choose a title')
                ]) ?>
            </div>
            <div class="col-md-6">
                <br/>
                <?= $form->field($model, 'status')->checkbox() ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12">
                <?php echo $form->field($model, 'selected_branches', [
                ])->widget(Select2::classname(), [

                    'data' => \backend\models\Branch::getBranchArray(),
                    //'maintainOrder' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'language' => Yii::$app->language,
                    'options' => [
                        'none-text' => Yii::t('backend', 'Choose Branches'),
                        'placeholder' => Yii::t('backend', 'Choose Branches'),
                        'multiple' => true,
                    ],
//                    'pluginOptions' => [
//                        'tags' => true,
//                        'tokenSeparators' => [','],
//                        'maximumInputLength' => 20
//                    ],
                ]); ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'director')->textInput(['maxlength' => 255]) ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'director_dni')->textInput(['maxlength' => 255]) ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'msisdn')->textInput() ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'msisdn_warning')->textarea() ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'address')->textarea([
                    'id' => 'address',
                    'rows' => 3,
                    'data-target' => '#addr-modal',
                    'data-toggle' => 'modal',
                    'readonly' => true,
                ]) ?>

            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'description')->textarea(['maxlength' => 500,'rows' => 3,]) ?>
            </div>
            <div class="clearfix"></div>


        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>
<div id="addr-modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><?= Yii::t('backend', 'Address') ?></h4>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'province_id')->widget(Select2::classname(), [
                            'data' => \yii\helpers\ArrayHelper::map(\backend\models\Area::getProvinceList(), 'province', 'name'),
                            'size' => Select2::MEDIUM,
                            'options' => [
                                'placeholder' => Yii::t('backend', 'Choose a province'),
                                'id' => 'province-id',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'addon' => [
//                                'prepend' => [
//                                    'content' => '<i class="glyphicon glyphicon-search"></i>'
//                                ]
                            ],
                        ])->label(Yii::t('backend', 'Province')); ?>

                        <?php

                        echo $form->field($model, 'district_id')->widget(DepDrop::classname(), [
                            'type' => DepDrop::TYPE_SELECT2,
                            'select2Options' => ['pluginOptions' => ['allowClear' => false]],
                            'data' => \yii\helpers\ArrayHelper::map(\backend\models\Area::getDistrictList($model->province_id), 'district', 'name'),
                            'options' => ['id' => 'district-id', 'none-text' => Yii::t('backend', 'Choose a district')],
                            'pluginOptions' => [
                                'loading' => false,
                                'depends' => ['province-id'],
                                'placeholder' => Yii::t('backend', 'Choose a district'),
                                'url' => \yii\helpers\Url::to(['area/get-district', 'selected' => $model->district_id]),
                            ]
                        ])->label(Yii::t('backend', 'District'));
                        ?>


                        <?php

                        echo $form->field($model, 'precinct_id')->widget(DepDrop::classname(), [
                            'type' => DepDrop::TYPE_SELECT2,
                            'select2Options' => ['pluginOptions' => ['allowClear' => false]],
                            'data' => \yii\helpers\ArrayHelper::map(\backend\models\Area::getPrecinctList($model->province_id, $model->district_id), 'precinct', 'name'),
                            'options' => ['id' => 'precinct-id', 'none-text' => Yii::t('backend', 'Choose a precinct')],
                            'pluginOptions' => [
                                'loading' => false,
                                'depends' => ['province-id', 'district-id'],
                                'placeholder' => Yii::t('backend', 'Choose a precinct'),
                                'url' => \yii\helpers\Url::to(['area/get-precinct', 'selected' => $model->precinct_id]),
                            ]
                        ])->label(Yii::t('backend', 'Precinct'));
                        ?>
                        <?= $form->field($model, 'address_text')->textInput(['id' => 'address-text', 'maxlength' => 255])->label(Yii::t('backend', 'Address')) ?>

                        <div id="addr-display" class="alert alert-info text-center" role="alert"></div>

                        <button class="btn btn-primary" type="button" id="save-addr"><?= Yii::t('backend', 'Accept') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

