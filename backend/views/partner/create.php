<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Partner */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Partner',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Partners'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row partner-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
