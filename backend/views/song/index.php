<?php


use \yii\grid\GridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SongSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Songs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row song-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>

                <div class="">
                    <?= Html::a(Yii::t('backend', 'Create {modelClass}', [
                        'modelClass' => Yii::t('backend', 'Song')
                    ]),
                        ['create'], ['class' => 'btn btn-info btn-sm']) ?>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <?php
                    Pjax::begin(['formSelector' => 'form', 'enablePushState' => false, 'id' => 'mainGridPjax']);
                    ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterSelector' => 'select[name="per-page"]',
                        'layout' => "{items}\n <div class='form-inline pagination page-size'>" . awesome\backend\grid\AwsPageSize::widget([
                                'options' => [
                                    'class' => 'form-control  form-control-sm',
                                ]]) . '</div> <div class="col-md-6">{pager}</div> <div class="pagination col-md-3 text-right total-count">' . Yii::t('backend', 'Tổng số') . ': <b>' . number_format($dataProvider->getTotalCount()) . '</b> ' . Yii::t('backend', 'bản ghi') . '</div>',
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'avatar',
                                'value' => function ($model) {
                                    return $model->getAvatarUrl();
                                },
                                'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                                'filter' => false,
                                'headerOptions' => ['style' => 'width:120px'],
                            ],
                            [
                                'attribute' => 'country_code',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    return $object->country? $object->country->country_name: null;
                                }
                            ],
                            'song_name',
                            [
                                'attribute' => 'author_id',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    return $object->author? $object->author->alias_name: null;
                                }
                            ],
                            [
                                'attribute' => 'is_active',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    $class = ($object->is_active == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                    return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                                }
                            ],
                            [
                                'attribute' => 'is_download',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    $class = ($object->is_download == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                    return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                                }
                            ],
                            //'avatar',
                            //'banner',
                            //'thumbnail',
                            // 'media_path',
                            // 'lyric:ntext',

                            // 'bit_rate',
                            // 'duration',
                            // 'slug',
                            // 'is_download',
//                             'total_listens:integer',
//                             'total_download:integer',
//                             'total_liked:integer',
//                             'total_shared:integer',
//                             'total_comment:integer',
                             'total_score:integer',
                            // 'number_of_artists',
                             'published_time',
                            'created_at',
                            'updated_at',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'headerOptions' => ['style' => 'width: 110px;', 'class' => 'head-actions'],
                                'contentOptions' => ['class' => 'row-actions'],
                                'buttons' => [
                                    'view' => function ($url, $model) {
                                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $model->id], [
                                            'class' => '',
                                            'data-target' => '#detail-modal',
                                            'data-toggle' => "modal"
                                        ]);
                                    },
                                ]
                            ],
                        ],
                    ]); ?>

                    <?php
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="detail-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">

        </div>

    </div>
</div>