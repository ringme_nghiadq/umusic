<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Song */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Songs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row song-view">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',

                        'country_code',
                        'song_name',
                        [
                            'attribute' => 'author_id',
                            'format' => 'raw',
                            'value' => function ($object) {
                                return $object->author? $object->author->alias_name: null;
                            }
                        ],
                        [
                            'attribute' => 'is_active',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->is_active == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],
                        [
                            'attribute' => 'is_download',
                            'format' => 'raw',
                            'value' => function ($object) {
                                $class = ($object->is_download == 1) ? 'glyphicon-ok' : 'glyphicon-remove';
                                return '<span class="glyphicon ' . $class . ' icon-is_active"></span>';
                            }
                        ],
                        [
                            'attribute' => 'thumbnail',
                            'value' => function ($model) {
                                return $model->getThumbnailUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:120px'],
                        ],
                        [
                            'attribute' => 'avatar',
                            'value' => function ($model) {
                                return $model->getAvatarUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:120px'],
                        ],
                        [
                            'attribute' => 'banner',
                            'value' => function ($model) {
                                return $model->getBannerUrl();
                            },
                            'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
                            'filter' => false,
                            'headerOptions' => ['style' => 'width:120px'],
                        ],

                        //'media_path',
                        'lyric:ntext',

                        'bit_rate',
                        'duration:integer',

                        'is_download:integer',
                        'total_listens:integer',
                        'total_download:integer',
                        'total_liked:integer',
                        'total_shared:integer',
                        'total_comment:integer',
                        'total_score:integer',
                        'number_of_artists:integer',
                        'published_time',
                        'created_at',
                        'updated_at',
                        'slug',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
