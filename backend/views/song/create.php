<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Song */

$this->title = Yii::t('backend', 'Create {modelClass}', [
    'modelClass' => 'Song',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Songs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row song-create">
    <div class="col-md-12">
        <?= $this->render('_form', [
            'model' => $model,
            'title' => $this->title
        ]) ?>
    </div>
</div>
