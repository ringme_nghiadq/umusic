<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;
use kartik\select2\Select2;
use \yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model backend\models\Song */
/* @var $title string */
/* @var $form ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="portlet light portlet-fit portlet-form bordered song-form">

    <div class="portlet-body">
        <div class="form-body row">
            <div class="col-md-6">
                <?= $form->field($model, 'country_code')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\MscCountry::find()->all(), 'country_code', 'country_name'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose a country'),
                        'id' => 'country-id'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'song_name')->textInput(['maxlength' => 255]) ?>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $form->field($model, 'author_id')->widget(Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\Author::find()->all(), 'id', 'alias_name'),
                    'size' => Select2::MEDIUM,
                    'options' => [
                        'placeholder' => Yii::t('backend', 'Choose a author'),
                        'id' => 'author-id'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'addon' => [
                        'prepend' => [
                            'content' => '<i class="glyphicon glyphicon-search"></i>'
                        ]
                    ],
                ]); ?>

                <?=
                $form->field($model, 'published_time', [
                    'template' => '{label}{input}{error}{hint}',
                ])->widget(\kartik\widgets\DateTimePicker::classname(), [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii:ss'
                    ]
                ]);
                ?>
                <br />
                <?= $form->field($model, 'is_active')->checkbox() ?>

                <?= $form->field($model, 'media_path')->fileInput() ?>
                <?php if (!$model->isNewRecord && $model->media_path): ?>
                    <audio controls >
                        <source src="<?= $model->getMediaPathUrl(); ?>" type="audio/mpeg">
                        Your browser does not support the audio element.
                    </audio>

                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php echo $form->field($model, 'selected_categories', [
                ])->widget(Select2::classname(), [

                    'data' => \backend\models\MscCategory::getCategoryArray(),
                    //'maintainOrder' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'language' => Yii::$app->language,
                    'options' => [
                        'none-text' => Yii::t('backend', 'Choose categories'),
                        'placeholder' => Yii::t('backend', 'Choose categories'),
                        'multiple' => true,
                    ],
                ]); ?>
                <?php
                $url = \yii\helpers\Url::to(['artist/ajax-search']);
                echo $form->field($model, 'selected_artists', [
                ])->widget(Select2::classname(), [

                    'data' => \yii\helpers\ArrayHelper::map($model->artists, 'id', 'alias_name'),
                    //'maintainOrder' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'language' => Yii::$app->language,
                    'options' => [
                        'none-text' => Yii::t('backend', 'Choose artists'),
                        'placeholder' => Yii::t('backend', 'Choose artists'),
                        'multiple' => true,
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => $url,
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(item) { return item.text; }'),
                        'templateSelection' => new JsExpression('function (item) { return item.text; }'),
                    ],
                ]); ?>


            </div>

            <div class="clearfix"></div>
            <div class="col-md-6">
                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'banner',
                    'itemName' => 'song-banner', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Banner'),
                    'dataMinSize' => '1080,368',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '1080:368',
                    'model' => $model,
                    'dataWillRemove' => 'headerWillChange',
                    'dataWillSave' => 'headerWillChange',
                    'helpBlock' => Yii::t('backend', 'Ratio 1080 x 368px (.jpg|png|jpeg)'),
                    'accept' => 'image/jpeg,image/jpg,image/png',
                ]) ?>




            </div>
            <div class="col-md-6">
                <?= $this->render('/common/_slim_image_field', [
                    'fieldName' => 'avatar',
                    'itemName' => 'song-avatar', // Vi du: adv, post, post-category de luu rieng tung folder
                    'fieldLabel' => Yii::t('backend', 'Avatar'),
                    'dataMinSize' => '100,100',
                    'dataSize' => '',
                    'dataForceSize' => '',
                    'dataRatio' => '100:100',
                    'model' => $model,
                    'dataWillRemove' => 'avatarWillChange',
                    'dataWillSave' => 'avatarWillChange',
                    'helpBlock' => Yii::t('backend', 'Ratio 100 x 100px(.jpg|png|jpeg)'),
                    'accept' => 'image/jpeg,image/jpg,image/png',
                ]) ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'lyric')->textarea(['rows' => 4]) ?>

            </div>




        </div>
    </div>
    <div class="portlet-title ">


        <a href="<?= \yii\helpers\Url::to(['index']); ?>" class="btn btn-default btn-sm">
            <i class="fa fa-angle-left"></i> <?= Yii::t('backend', 'Back') ?>                </a>
        &nbsp;&nbsp;&nbsp;
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => 'btn btn-transparent green  btn-sm']) ?>


    </div>
</div>

<?php ActiveForm::end(); ?>
<?php \common\components\slim\SlimAsset::register($this); ?>
