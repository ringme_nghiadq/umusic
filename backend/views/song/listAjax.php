<?php


use \yii\grid\GridView;
use awesome\backend\widgets\AwsBaseHtml;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SongSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Songs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row song-index">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-body">
                <div class="table-container">
                    <?php
                    Pjax::begin(['timeout' => 0, 'formSelector' => 'form#song-search-ajax', 'enablePushState' => false, 'id' => 'songGridPjax']);
                    ?>

                    <?= GridView::widget([
                        'pager' => [
                            'maxButtonCount'=> 5,
                        ],
                        'dataProvider' => $dataProvider,
                        'filterSelector' => 'select[name="per-page"]',
                        'layout' => "{items}\n " . ' <div class="col-md-6">{pager}</div> <div class="pagination col-md-3 text-right total-count">' . Yii::t('backend', 'Tổng số') . ': <b>' . number_format($dataProvider->getTotalCount()) . '</b> ' . Yii::t('backend', 'bản ghi') . '</div>',
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],
//                            [
//                                'attribute' => 'avatar',
//                                'value' => function ($model) {
//                                    return $model->getAvatarUrl();
//                                },
//                                'format' => ['image', ['height' => '80', 'onerror' => "this.src='" . Yii::$app->params['no_image'] . "';"]],
//                                'filter' => false,
//                                'headerOptions' => ['style' => 'width:120px'],
//                            ],
                            [
                                'attribute' => '#',
                                'format' => 'raw',
                                'value' => function ($object) use ($ctype, $ctypeId) {

                                    return Html::button(Yii::t('backend', 'Add'), [
                                        'ahref' => \yii\helpers\Url::to([
                                            'song/add-to-collection',
                                            'ctype' => $ctype,
                                            'ctype_id' => $ctypeId,
                                            'song_id' => $object->id,
                                        ]),
                                        'class' => 'btn btn-sm btn-primary btn-song-add',
                                        'song_id' => $object->id,
                                    ]);
                                }
                            ],
                            [
                                'attribute' => 'country_code',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    return $object->country? $object->country->country_name: null;
                                }
                            ],
                            'song_name',
                            [
                                'attribute' => 'author_id',
                                'format' => 'raw',
                                'value' => function ($object) {
                                    return $object->author? $object->author->alias_name: null;
                                }
                            ],


                            //'avatar',
                            //'banner',
                            //'thumbnail',
                            // 'media_path',
                            // 'lyric:ntext',

                            // 'bit_rate',
                            // 'duration',
                            // 'slug',
                            // 'is_download',
//                             'total_listens:integer',
//                             'total_download:integer',
//                             'total_liked:integer',
//                             'total_shared:integer',
//                             'total_comment:integer',
                            //'total_score:integer',
                            // 'number_of_artists',
                            'published_time',


                        ],
                    ]); ?>

                    <?php
                    Pjax::end();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
