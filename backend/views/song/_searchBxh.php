<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\SongSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default song-search">
    <div class="panel-body row">
        <?php
        switch ($model->collection_type) {
            case 'ranking_tab':
                $action = 'bxh';
                break;
            case 'new_tab':
                $action = 'new-tab';
                break;
            default:
                $action = 'index';
        }
        ?>
        <?php $form = ActiveForm::begin([
            'action' => $action,
            'method' => 'get',
        ]); ?>

        <div class="col-md-3">

            <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\MscCategory::find()
                    ->where(['id' => $categoryIds ])
                    ->all(), 'id', 'cate_name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'All'),
                    'id' => 'cate-id'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'country_code')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\MscCountry::find()->all(), 'country_code', 'country_name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'All'),
                    'id' => 'country-id'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'song_name') ?>

        </div>
        <div class="col-md-3">
            <?php echo
            $form->field($model, 'is_active')->dropDownList(
                \common\helpers\Helpers::commonStatusArr(),
                ['prompt' => Yii::t('backend', 'All')]
            );;

            ?>

        </div>
        <div class="col-md-3">
            <?php echo

            $form->field($model, 'published_time')->widget(\kartik\daterange\DateRangePicker::className(), [
                    'options' => ['class' => 'form-control daterange-field input-sm'],
                    'model' => $model,
                    'attribute' => 'published_time',
                    'convertFormat' => true,
                    'presetDropdown' => true,
                    'readonly' => true,
                    'pluginOptions' => [
                        'opens' => 'left',
                        'alwaysShowCalendars' => true,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'd/m/Y',
                        ]
                    ],
                    'pluginEvents' => [
                        'cancel.daterangepicker' => "function(ev, picker) {
                                            $(this).val('');
                                        }",
                        'apply.daterangepicker' => 'function(ev, picker) {
                                            if($(this).val() == "") {
                                                $(this).val(picker.startDate.format(picker.locale.format) + picker.locale.separator +
                                                picker.endDate.format(picker.locale.format)).trigger("change");
                                            }
                                        }',
                        'show.daterangepicker' => 'function(ev, picker) {
                                            picker.container.find(".ranges").off("mouseenter.daterangepicker", "li");
                                            if($(this).val() == "") {
                                                picker.container.find(".ranges .active").removeClass("active");
                                            }
                                        }',
                    ]
                ]
            )
            ?>

        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'author_id')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\Author::find()->all(), 'id', 'alias_name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'All'),
                    'id' => 'author-id'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'artist_id')->widget(Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\Artist::find()->all(), 'id', 'alias_name'),
                'size' => Select2::MEDIUM,
                'options' => [
                    'placeholder' => Yii::t('backend', 'All'),
                    'id' => 'artist-id'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ])->label(Yii::t('backend', 'Artist')); ?>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::a(Yii::t('backend', 'Reset'), [$action], ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
