<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model backend\models\SongSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="panel panel-default song-search">
    <div class="panel-body row">

        <?php $form = ActiveForm::begin([
            'action' => ['/song/list-ajax', 'ctype' => $ctype, 'ctype_id' => $ctypeId],
            'method' => 'get',
            'id' => 'song-search-ajax',
        ]); ?>



<!--        <div class="col-md-3">-->
        <p><div class="col-md-3"><?= $form->field($model, 'song_name') ?></div>
        <div class="col-md-3"><?= $form->field($model, 'author_id')->widget(Select2::className(), [
                'data' => Yii\helpers\ArrayHelper::map(backend\models\Author::find()->all(), 'id', 'alias_name'),
                'options' => [
                    'placeholder' => Yii::t('backend', 'All'),
                    'id' => 'author-id'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => '<i class="glyphicon glyphicon-search"></i>'
                    ]
                ],
            ]) ?></div>
        </p>
<!--        </div>-->


        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
                &nbsp;&nbsp;&nbsp;
                <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
