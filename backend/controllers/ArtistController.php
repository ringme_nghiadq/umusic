<?php

namespace backend\controllers;

use Yii;
use backend\models\Artist;
use backend\models\ArtistSearch;
use backend\models\Song;
use backend\models\SongSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;
use common\helpers\FileHelper;

/**
 * ArtistController implements the CRUD actions for Artist model.
 */
class ArtistController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Artist models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArtistSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Artist model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new Artist model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Artist();
        $model->is_active = Artist::STATUS_APPROVED;

        $modelClassName = Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate
        $form_values['avatar'] = Slim::getImagesFromSlimRequest('avatar');
        $form_values['banner'] = Slim::getImagesFromSlimRequest('banner');

        if ($model->load($form_values) && $model->save()) {

            FileHelper::processUploadNewImage($model, 'avatar');
            FileHelper::processUploadNewImage($model, 'banner');

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }

        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing Artist model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName = Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        // Lay du lieu anh upload len de validate
        $form_values['avatar'] = Slim::getImagesFromSlimRequest('avatar');
        $form_values['banner'] = Slim::getImagesFromSlimRequest('banner');

        if ($model->load($form_values) && $model->save()) {

            FileHelper::processUploadNewImage($model, 'avatar');
            FileHelper::processUploadNewImage($model, 'banner');

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Artist model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelClassName = Inflector::camel2words(StringHelper::basename($model::className()));
        $model->is_active = Artist::STATUS_DELETE;
        $model->save();

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Artist model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Artist the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Artist::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSong($id)
    {
        $searchModel = new SongSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);
        return $this->render('song', [
            'model' => $model,
            'songs' => $model->songs,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjaxSearch($q = null, $ids = [])
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data = Artist::find()
                ->select('id, alias_name as text')
                ->where(['like', 'alias_name', $q])
                ->limit(20)
                ->asArray()
                ->all();

            $out['results'] = array_values($data);
        } elseif (is_array($ids) && count($ids)) {
            $data = Artist::find()
                ->select('id, alias_name AS text')
                ->where(['id' => $ids])
                ->asArray()
                ->all();
            $out['results'] = array_values($data);
        }
        return $out;

    }


}
