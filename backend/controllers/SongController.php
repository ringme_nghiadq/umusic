<?php

namespace backend\controllers;

use backend\models\Album;
use backend\models\Artist;
use backend\models\MscShowOn;
use backend\models\Playlist;
use backend\models\SongAlbum;
use backend\models\SongArtist;
use common\helpers\FileHelper;
use common\models\SongBase;
use frontend\models\SongPlaylist;
use Yii;
use backend\models\Song;
use backend\models\SongSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * SongController implements the CRUD actions for Song model.
 */
class SongController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Song models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SongSearch(['show_deleted_songs' => false]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionBxh()
    {
        $searchModel = new SongSearch();
        $searchModel->collection_type = 'ranking_tab';
        $categoryIds = ArrayHelper::map(MscShowOn::find()
            ->select('content_id')
            ->where([
                'position_key' => $searchModel->collection_type,
                'content_type' => 'category',
            ])
            ->all(), 'content_id', 'content_id')
            ;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('bxh', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categoryIds' => $categoryIds,
        ]);
    }

    public function actionNewTab()
    {
        $searchModel = new SongSearch();
        $searchModel->collection_type = 'new_tab';

        $categoryIds = ArrayHelper::map(MscShowOn::find()
            ->select('content_id')
            ->where([
                'position_key' => $searchModel->collection_type,
                'content_type' => 'category',
            ])
            ->all(), 'content_id', 'content_id')
        ;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('bxh', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'categoryIds' => $categoryIds,
        ]);
    }

    /**
     * Displays a single Song model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new Song model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Song();
        $model->is_active = Song::STATUS_APPROVED;
        $model->published_time = date('Y-m-d H:i:s');

        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();

        $form_values['avatar'] = Slim::getImagesFromSlimRequest('avatar');
        $form_values['banner'] = Slim::getImagesFromSlimRequest('banner');

        if ($model->load($form_values)) {

            $model->media_path = $videoMedia = UploadedFile::getInstance($model, 'media_path');

            if ($model->save()) {
                if ($model->uploadMedia()) {
                    // upload thanh cong, goi api convert
                }

                FileHelper::processUploadNewImage($model, 'avatar');
                FileHelper::processUploadNewImage($model, 'banner');

                Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', $modelClassName)]));

                // bo sung redirect
                if (Yii::$app->request->post('save_and_back')) {
                    return $this->redirect(['index']);
                } elseif (Yii::$app->request->post('save_and_add')) {
                    return $this->redirect(['create']);
                } else {
                    return $this->redirect(['update', 'id' => $model->id]);
                }
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing Song model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        $form_values['avatar'] = Slim::getImagesFromSlimRequest('avatar');
        $form_values['banner'] = Slim::getImagesFromSlimRequest('banner');

        if ($model->load($form_values)) {

            if (!$model->media_path) {
                $model->media_path = $model->getOldAttribute('media_path');
            }

            if ($model->save()) {
                $model->media_path = UploadedFile::getInstance($model, 'media_path');

                if ($model->media_path) {

                    if ($model->uploadMedia()) {
                        // upload thanh cong, goi api convert

                    }
                }

                FileHelper::processUpdateImage($model, $form_values, 'avatar');
                FileHelper::processUpdateImage($model, $form_values, 'banner');

                Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

                // bo sung redirect
                if (Yii::$app->request->post('save_and_back')) {
                    return $this->redirect(['index']);
                } elseif (Yii::$app->request->post('save_and_add')) {
                    return $this->redirect(['create']);
                } else {
                    return $this->redirect(['update', 'id' => $model->id]);
                }
            } else {
                //var_dump( $model->getErrors());die;
            }

        } else {

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Song model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->is_active = SongBase::STATUS_DELETE;

        $model->save(false, ['is_active']);

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Delete {object} successfully!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Song model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Song the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Song::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListAjax()
    {
        $this->layout = false;

        // ctype = album, playlist, category
        $ctype = Yii::$app->request->get('ctype', null);
        $ctypeId = Yii::$app->request->get('ctype_id', null);

        $searchModel = new SongSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setPagination([
            'pageSize' => 10,

        ]);
        return $this->render('listAjax', [
            'dataProvider' => $dataProvider,
            'ctype' => $ctype,
            'ctypeId' => $ctypeId,
        ]);
    }

    /**
     * them vao danh sach
     * @param $ctype
     * @param $ctype_id
     * @param $song_id
     * @return array
     */
    public function actionAddToCollection($ctype, $ctype_id, $song_id)
    {
        $this->layout = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resp = [
            'error_code' => 404,
            'message' => Yii::t('backend', 'Not found')
        ];

        switch ($ctype) {
            case 'album':
                $album = Album::findOne($ctype_id);
                if ($album) {
                    $sa = new SongAlbum();
                    $sa->album_id  = $ctype_id;
                    $sa->song_id = $song_id;
                    $sa->save();

                    $resp = [
                        'error_code' => 0,
                        'message' => Yii::t('backend', 'Add song successfully!')
                    ];
                    return $resp;
                }
                break;
            case 'playlist':
                $playlist = Playlist::findOne($ctype_id);
                if ($playlist) {
                    $sa = new SongPlaylist();
                    $sa->playlist_id  = $ctype_id;
                    $sa->song_id = $song_id;
                    $sa->save();

                    $resp = [
                        'error_code' => 0,
                        'message' => Yii::t('backend', 'Add song successfully!')
                    ];
                    return $resp;
                }
                break;
            case 'artist':
                $playlist = Artist::findOne($ctype_id);
                if ($playlist) {
                    $sa = new SongArtist();
                    $sa->artist_id  = $ctype_id;
                    $sa->song_id = $song_id;
                    $sa->save();

                    $resp = [
                        'error_code' => 0,
                        'message' => Yii::t('backend', 'Add song successfully!')
                    ];
                    return $resp;
                }
                break;
            default:

                return $resp;
        }

    }

    public function actionRemoveToCollection($ctype, $ctype_id, $song_id)
    {
        $this->layout = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resp = [
            'error_code' => 404,
            'message' => Yii::t('backend', 'Not found')
        ];

        switch ($ctype) {
            case 'album':
                $album = Album::findOne($ctype_id);
                if ($album) {
                    /*$sa = SongAlbum::deleteAll([
                        'album_id' => $ctype_id,
                        'song_id' => $song_id,
                    ]);*/
                    $albumSongs = SongAlbum::findAll([
                        'album_id' => $ctype_id,
                        'song_id' => $song_id,
                    ]);
                    foreach ($albumSongs as $albumSong) {
                        $albumSong->delete();
                    }

                    $resp = [
                        'error_code' => 0,
                        'message' => Yii::t('backend', 'Remove song successfully!')
                    ];
                    return $resp;
                }
                break;
            case 'playlist':
                $playlist = Playlist::findOne($ctype_id);
                if ($playlist) {
                    $sa = SongPlaylist::deleteAll([
                        'playlist_id' => $ctype_id,
                        'song_id' => $song_id,
                    ]);

                    $resp = [
                        'error_code' => 0,
                        'message' => Yii::t('backend', 'Remove song successfully!')
                    ];
                    return $resp;
                }
                break;
            case 'artist':
                $playlist = Artist::findOne($ctype_id);
                if ($playlist) {
                    $sa = SongArtist::deleteAll([
                        'artist_id' => $ctype_id,
                        'song_id' => $song_id,
                    ]);

                    $resp = [
                        'error_code' => 0,
                        'message' => Yii::t('backend', 'Remove song successfully!')
                    ];
                    return $resp;
                }
                break;
            default:

                return $resp;
        }

    }

    public function actionAjaxSearch($q = null, $ids = [])
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data = Song::find()
                ->select('id, song_name as text')
                ->where(['like', 'song_name', $q])
                ->limit(20)
                ->asArray()
                ->all();

            $out['results'] = array_values($data);
        } elseif (is_array($ids) && count($ids)) {
            $data = Song::find()
                ->select('id, song_name AS text')
                ->where(['id' => $ids])
                ->asArray()
                ->all();
            $out['results'] = array_values($data);
        }
        return $out;
    }
}
