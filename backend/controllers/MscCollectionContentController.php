<?php

namespace backend\controllers;

use Yii;
use backend\models\MscCollectionContent;
use backend\models\MscCollectionContentSearch;
use backend\models\MscShowOn;
use common\models\AlbumBase;
use common\models\PlaylistBase;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use common\components\slim\Slim;

/**
 * MscCollectionContentController implements the CRUD actions for MscCollectionContent model.
 */
class MscCollectionContentController extends Controller
{
    public function actionPersonalTab()
    {
        $searchModel = new MscCollectionContentSearch();
        Yii::$app->view->title = Yii::t('backend', 'Personal tab');
        $collectionIds = ArrayHelper::map(MscShowOn::find()
            ->select('content_id')
            ->where([
                'position_key' => 'personal_tab',
                'content_type' => 'collection',
            ])
            ->all(), 'content_id', 'content_id')
        ;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'collectionIds' => $collectionIds,
            'page' => 'personal-tab',
        ]);
    }

    public function actionGenreTab()
    {
        $searchModel = new MscCollectionContentSearch();
        Yii::$app->view->title = Yii::t('backend', 'Genre tab');
        $collectionIds = ArrayHelper::map(MscShowOn::find()
            ->select('content_id')
            ->where([
                'position_key' => 'genre_tab',
                'content_type' => 'collection',
            ])
            ->all(), 'content_id', 'content_id')
        ;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'collectionIds' => $collectionIds,
            'page' => 'genre-tab',
        ]);
    }
}
