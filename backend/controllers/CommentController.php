<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\filters\VerbFilter;
use backend\models\MscAlbumCommentInfoSearch;
use backend\models\MscSongCommentInfoSearch;
use backend\models\MscArtistCommentInfoSearch;
use backend\models\MscAuthorCommentInfoSearch;
use backend\models\MscPlaylistCommentInfoSearch;

class CommentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionAlbumComment()
    {
        $searchModel = new MscAlbumCommentInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->view->title = Yii::t('backend', 'Album comment');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page' => 'album-comment',
        ]);
    }

    public function actionSongComment()
    {
        $searchModel = new MscSongCommentInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->view->title = Yii::t('backend', 'Song comment');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page' => 'song-comment',
        ]);
    }

    public function actionArtistComment()
    {
        $searchModel = new MscArtistCommentInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->view->title = Yii::t('backend', 'Artist comment');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page' => 'artist-comment',
        ]);
    }

    public function actionAuthorComment()
    {
        $searchModel = new MscAuthorCommentInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->view->title = Yii::t('backend', 'Author comment');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page' => 'author-comment',
        ]);
    }

    public function actionPlaylistComment()
    {
        $searchModel = new MscPlaylistCommentInfoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->view->title = Yii::t('backend', 'Playlist comment');

        return $this->render('playlistComment', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page' => 'playlist-comment',
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
//        var_dump($model->getCollection()->name); die;
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->delete();
        Yii::$app->session->setFlash('info', Yii::t('backend', 'Delete {object} successfully!', ['object' => Yii::t('backend', $modelClassName)]));
        return $this->redirect([$this->getPageByCollection($model->getCollection()->name)]);
    }

    protected function findModel($id)
    {
        if (($model = \common\models\MscAlbumCommentInfo::findOne($id)) !== null) {
            return $model;
        }
        elseif(($model = \common\models\MscArtistCommentInfo::findOne($id)) !== null) {
            return $model;
        }
        elseif(($model = \common\models\MscAuthorCommentInfo::findOne($id)) !== null) {
            return $model;
        }
        elseif(($model = \common\models\MscSongCommentInfo::findOne($id)) !== null) {
            return $model;
        }
        elseif(($model = \common\models\MscPlaylistCommentInfo::findOne($id)) !== null) {
            return $model;
        }
        else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function getPageByCollection($collectionName) {
        $data = [
            MscAlbumCommentInfoSearch::collectionName() => 'album-comment',
            MscSongCommentInfoSearch::collectionName() => 'song-comment',
        ];
        return $data[$collectionName];
    }
}
