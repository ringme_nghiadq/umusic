<?php

namespace backend\controllers;

use backend\models\Album;
use backend\models\AlbumSearch;
use backend\models\MscCategory;
use backend\models\MscCategorySearch;
use backend\models\Playlist;
use backend\models\PlaylistSearch;
use Yii;
use backend\models\MscCollection;
use backend\models\MscCollectionSearch;
use yii\db\ActiveQuery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;

/**
 * MscCollectionController implements the CRUD actions for MscCollection model.
 */
class MscCollectionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MscCollection models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MscCollectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MscCollection model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new MscCollection model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MscCollection();
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate
        // Neu ko co thi xoa
        $imageFieldName = 'image_path';
        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);
        if ($model->load($form_values) && $model->save()) {
            $images = Slim::getImages($imageFieldName);

            if($images != false && sizeof($images) > 0) {

                $imagePath = Slim::saveSlimImage('backend', Inflector::underscore(StringHelper::basename($model::className())), $images[0]);

                $model->$imageFieldName = $imagePath;
                $model->save();
            }

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing MscCollection model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        // Lay du lieu anh upload len de validate
        $imageFieldName = 'image_path';
        $form_values[$imageFieldName] = Slim::getImagesFromSlimRequest($imageFieldName);

        if ($model->load($form_values) && $model->save()) {

            if ($form_values['change_'. $imageFieldName] == '1') {
                $oldImagePath = ($model->$imageFieldName)? Yii::getAlias('@backend_web'). DIRECTORY_SEPARATOR. $model->$imageFieldName: null;
                $images = Slim::getImages($imageFieldName);

                if($images != false && sizeof($images) > 0) {
                    $imagePath = Slim::saveSlimImage('backend', Inflector::underscore(StringHelper::basename($model::className())), $images[0]);
                    $oldAltPath = $model->$imageFieldName;

                    $model->$imageFieldName = $imagePath;
                    $model->save();

                    if ($oldAltPath != $imagePath) {
                        // Xoa file anh cu
                        if ($oldImagePath && file_exists($oldImagePath))
                            unlink($oldImagePath);

                    }
                } else {
                    $model->$imageFieldName = null;

                    $model->save();
                    // Xoa file anh cu
                    if ($oldImagePath && file_exists($oldImagePath))
                        unlink($oldImagePath);
                }
            }

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MscCollection model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->is_active = MscCollection::STATUS_DELETE;
        $model->save();

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the MscCollection model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MscCollection the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MscCollection::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionAlbum($id)
    {
        $searchModel = new AlbumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);


        $albums = Album::find()
            ->alias('a')
            -> innerJoin('msc_collection_content c', '(c.content_id=a.id AND c.content_type = :type AND c.collection_id = :id)', [
                'type' => 'album',
                'id' => $id,
            ])
            //->orderBy('c.created_at DESC')
            ->all();

        return $this->render('album', [
            'model' => $model,
            'albums' => $albums,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPlaylist($id)
    {
        $searchModel = new PlaylistSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);


        $playlists = Playlist::find()
            ->alias('a')
            -> innerJoin('msc_collection_content c', '(c.content_id=a.id AND c.content_type = :type AND c.collection_id = :id)', [
                'type' => 'playlist',
                'id' => $id,
            ])
            //->orderBy('c.created_at DESC')
            ->all();

        return $this->render('playlist', [
            'model' => $model,
            'playlists' => $playlists,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCategory($id)
    {
        $searchModel = new MscCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);


        $list = MscCategory::find()
            ->alias('a')
            -> innerJoin('msc_collection_content c', '(c.content_id=a.id AND c.content_type = :type AND c.collection_id = :id)', [
                'type' => 'category',
                'id' => $id,
            ])
            //->orderBy('c.created_at DESC')
            ->all();

        return $this->render('category', [
            'model' => $model,
            'list' => $list,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
