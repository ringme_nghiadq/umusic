<?php

namespace backend\controllers;


use backend\models\MscCollection;
use backend\models\MscCollectionContent;
use backend\models\Song;
use backend\models\SongSearch;
use Yii;
use backend\models\Album;
use backend\models\AlbumSearch;
use yii\data\Pagination;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use common\components\slim\Slim;
use common\helpers\FileHelper;
use yii\web\Response;

/**
 * AlbumController implements the CRUD actions for Album model.
 */
class AlbumController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Album models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AlbumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Album model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new Album model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Album();
        $model->is_active = Album::STATUS_APPROVED;
        $model->published_time = date('Y-m-d H:i:s');

        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();
        // Lay du lieu anh upload len de validate

        $form_values['avatar'] = Slim::getImagesFromSlimRequest('avatar');
        $form_values['banner'] = Slim::getImagesFromSlimRequest('banner');

        if ($model->load($form_values) && $model->save()) {

            FileHelper::processUploadNewImage($model, 'avatar');
            FileHelper::processUploadNewImage($model, 'banner');

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing Album model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        // Lay du lieu anh upload len de validate
        $form_values['avatar'] = Slim::getImagesFromSlimRequest('avatar');
        $form_values['banner'] = Slim::getImagesFromSlimRequest('banner');

        if ($model->load($form_values) && $model->save()) {

            FileHelper::processUpdateImage($model, $form_values, 'avatar');
            FileHelper::processUpdateImage($model, $form_values, 'banner');

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Album model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->is_active = Album::STATUS_DELETE;
        $model->delete();

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Album model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Album the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Album::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSong($id)
    {
//        $query = Song::find()
//            ->alias('a')
//            ->joinWith([
//                'song_album' => function($query) use ($id) {
//
//                    $query->where(['=', 'song_album.album_id', $id]);
//                },
//            ])
//            ->andWhere('a.is_active = 1')
//            ->orderBy('a.name DESC');
//
//        $countQuery = clone $query;
//        // get the total number of articles (but do not fetch the article data yet)
//        $count = $countQuery->count();
//
//        // create a pagination object with the total count
//        $pages = new Pagination([
//            'totalCount' => $count,
//            'defaultPageSize' => 12,
//        ]);
//
//        // limit the query using the pagination and retrieve the articles
//        $songPager = $query->offset($pages->offset)
//            ->limit($pages->limit)
//            ->all();

        $searchModel = new SongSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);
        return $this->render('song', [
            'model' => $model,
            'songs' => $model->songs,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionListAjax()
    {
        $this->layout = false;

        // ctype = album, playlist, category
        $ctype = Yii::$app->request->get('ctype', null);
        $ctypeId = Yii::$app->request->get('ctype_id', null);

        $searchModel = new AlbumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setPagination([
            'pageSize' => 10,

        ]);
        return $this->render('listAjax', [
            'dataProvider' => $dataProvider,
            'ctype' => $ctype,
            'ctypeId' => $ctypeId,
        ]);
    }

    /**
     * them vao danh sach
     * @param $ctype
     * @param $ctype_id
     * @param $album_id
     * @return array
     */
    public function actionAddToCollection($ctype, $ctype_id, $album_id)
    {
        $this->layout = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resp = [
            'error_code' => 404,
            'message' => Yii::t('backend', 'Not found')
        ];

        switch ($ctype) {
            case 'collection':
                $collection = MscCollection::findOne($ctype_id);
                if ($collection) {
                    $collectionContent = new MscCollectionContent();
                    $collectionContent->collection_id  = $ctype_id;
                    $collectionContent->content_id = $album_id;
                    $collectionContent->content_type = 'album';
                    $collectionContent->save();

                    $resp = [
                        'error_code' => 0,
                        'message' => Yii::t('backend', 'Add album successfully!')
                    ];
                    return $resp;
                }
                break;

            default:

                return $resp;
        }

    }

    public function actionRemoveToCollection($ctype, $ctype_id, $album_id)
    {
        $this->layout = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resp = [
            'error_code' => 404,
            'message' => Yii::t('backend', 'Not found')
        ];

        switch ($ctype) {
            case 'collection':
                $collection = MscCollection::findOne($ctype_id);
                if ($collection) {
                    $sa = MscCollectionContent::deleteAll([
                        'collection_id' => $ctype_id,
                        'content_id' => $album_id,
                        'content_type' => 'album',
                    ]);

                    $resp = [
                        'error_code' => 0,
                        'message' => Yii::t('backend', 'Remove album successfully!')
                    ];
                    return $resp;
                }
                break;

            default:

                return $resp;
        }

    }

    public function actionAjaxSearch($q = null, $ids = [])
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $data = Album::find()
                ->select('id, album_name as text')
                ->where(['like', 'album_name', $q])
                ->limit(20)
                ->asArray()
                ->all();

            $out['results'] = array_values($data);
        } elseif (is_array($ids) && count($ids)) {
            $data = Album::find()
                ->select('id, album_name AS text')
                ->where(['id' => $ids])
                ->asArray()
                ->all();
            $out['results'] = array_values($data);
        }
        return $out;
    }
}
