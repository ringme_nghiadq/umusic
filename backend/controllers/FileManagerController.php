<?php

namespace backend\controllers;

use yii\helpers\FileHelper;
use yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

class FileManagerController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = false;

        $rootPath = Yii::$app->params['ftp_upload_path'];
        $parent = Yii::$app->request->get('parent');
        $isRoot = false;

        if (!$parent || strpos($parent, $rootPath) === false) {
            $parent = $rootPath;
            $isRoot = true;
        }

        $folders = FileHelper::findDirectories($parent, ['recursive' => false]);

        //mp4,mov,mpeg,avi,wmv,flv,3gpp,webm
        $files = FileHelper::findFiles($parent, [
            'recursive' => false,
            'only'=>['*.mp4','*.mov', '*.mpeg', '*.avi', '*.wmv', '*.flv', '*.3gpp', '*.webm']
        ]);


        return $this->render((($isRoot)? 'index': 'child'), [
            'folders' => $folders,
            'files' => $files,
            'parent' => $parent,
        ]);
    }

}
