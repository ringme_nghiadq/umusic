<?php

namespace backend\controllers;

use backend\models\Album;
use backend\models\AlbumSearch;
use backend\models\Song;
use backend\models\SongSearch;
use backend\models\Playlist;
use backend\models\PlaylistSearch;
use backend\models\Artist;
use backend\models\ArtistSearch;
use backend\models\MscCollection;
use backend\models\MscCollectionContent;
use Yii;
use backend\models\MscCategory;
use backend\models\MscCategorySearch;
use \common\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use common\components\slim\Slim;
use yii\web\Response;

/**
 * MscCategoryController implements the CRUD actions for MscCategory model.
 */
class MscCategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all MscCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MscCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MscCategory model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            $this->layout = false;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'isAjax' => Yii::$app->request->isAjax,
        ]);
    }

    /**
     * Creates a new MscCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MscCategory();
        $model->is_active = 1;

        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $form_values = Yii::$app->request->post();

        $form_values['avatar'] = Slim::getImagesFromSlimRequest('avatar');
        $form_values['banner'] = Slim::getImagesFromSlimRequest('banner');

        if ($model->load($form_values) && $model->save()) {
            $model->saveCountries();
            FileHelper::processUploadNewImage($model, 'avatar');
            FileHelper::processUploadNewImage($model, 'banner');

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Thêm mới "{object}" thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }

        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }


    }

    /**
     * Updates an existing MscCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));

        $model->loadDefaultValues();
        $form_values = Yii::$app->request->post();

        $form_values['avatar'] = Slim::getImagesFromSlimRequest('avatar');
        $form_values['banner'] = Slim::getImagesFromSlimRequest('banner');

        if ($model->load($form_values) && $model->save()) {
            $model->saveCountries();

            FileHelper::processUpdateImage($model, $form_values, 'avatar');
            FileHelper::processUpdateImage($model, $form_values, 'banner');

            Yii::$app->session->setFlash('info', Yii::t('backend', 'Chỉnh sửa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

            // bo sung redirect
            if (Yii::$app->request->post('save_and_back')) {
                return $this->redirect(['index']);
            } elseif (Yii::$app->request->post('save_and_add')) {
                return $this->redirect(['create']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing MscCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $modelClassName =  Inflector::camel2words(StringHelper::basename($model::className()));
        $model->is_active = MscCategory::STATUS_DELETE;
        $model->save();

        Yii::$app->session->setFlash('info', Yii::t('backend', 'Xóa {object} thành công!', ['object' => Yii::t('backend', $modelClassName)]));

        return $this->redirect(['index']);
    }

    /**
     * Finds the MscCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MscCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MscCategory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionListAjax()
    {
        $this->layout = false;

        // ctype = collection
        $ctype = Yii::$app->request->get('ctype', null);
        $ctypeId = Yii::$app->request->get('ctype_id', null);

        $searchModel = new MscCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setPagination([
            'pageSize' => 10,

        ]);
        return $this->render('listAjax', [
            'dataProvider' => $dataProvider,
            'ctype' => $ctype,
            'ctypeId' => $ctypeId,
        ]);
    }

    /**
     * them vao danh sach
     * @param $ctype
     * @param $ctype_id
     * @param $category_id
     * @return array
     */
    public function actionAddToCollection($ctype, $ctype_id, $category_id)
    {
        $this->layout = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resp = [
            'error_code' => 404,
            'message' => Yii::t('backend', 'Not found')
        ];

        switch ($ctype) {
            case 'collection':
                $collection = MscCollection::findOne($ctype_id);
                if ($collection) {
                    $collectionContent = new MscCollectionContent();
                    $collectionContent->collection_id  = $ctype_id;
                    $collectionContent->content_id = $category_id;
                    $collectionContent->content_type = 'category';
                    $collectionContent->save();

                    $resp = [
                        'error_code' => 0,
                        'message' => Yii::t('backend', 'Add category successfully!')
                    ];
                    return $resp;
                }
                break;

            default:

                return $resp;
        }

    }

    public function actionRemoveToCollection($ctype, $ctype_id, $category_id)
    {
        $this->layout = false;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $resp = [
            'error_code' => 404,
            'message' => Yii::t('backend', 'Not found')
        ];

        switch ($ctype) {
            case 'collection':
                $collection = MscCollection::findOne($ctype_id);
                if ($collection) {
                    $sa = MscCollectionContent::deleteAll([
                        'collection_id' => $ctype_id,
                        'content_id' => $category_id,
                        'content_type' => 'category',
                    ]);

                    $resp = [
                        'error_code' => 0,
                        'message' => Yii::t('backend', 'Remove playlist successfully!')
                    ];
                    return $resp;
                }
                break;

            default:

                return $resp;
        }

    }


    public function actionPlaylist($id)
    {
        $searchModel = new PlaylistSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);


        $playlists = Playlist::find()
            ->alias('a')
            -> innerJoin('msc_category_content c', '(c.content_id=a.id AND c.content_type = :type AND c.category_id = :id)', [
                'type' => 'playlist',
                'id' => $id,
            ])
            //->orderBy('c.created_at DESC')
            ->all();

        return $this->render('playlist', [
            'model' => $model,
            'playlists' => $playlists,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSong($id)
    {
        $searchModel = new SongSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);


        $list = Song::find()
            ->alias('a')
            -> innerJoin('msc_category_content c', '(c.content_id=a.id AND c.content_type = :type AND c.category_id = :id)', [
                'type' => 'song',
                'id' => $id,
            ])
            //->orderBy('c.created_at DESC')
            ->all();

        return $this->render('song', [
            'model' => $model,
            'list' => $list,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAlbum($id)
    {
        $searchModel = new AlbumSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);


        $albums = Album::find()
            ->alias('a')
            -> innerJoin('msc_category_content c', '(c.content_id=a.id AND c.content_type = :type AND c.category_id = :id)', [
                'type' => 'album',
                'id' => $id,
            ])
            //->orderBy('c.created_at DESC')
            ->all();

        return $this->render('album', [
            'model' => $model,
            'albums' => $albums,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionArtist($id)
    {
        $searchModel = new ArtistSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = $this->findModel($id);


        $list = Song::find()
            ->alias('a')
            -> innerJoin('msc_category_content c', '(c.content_id=a.id AND c.content_type = :type AND c.category_id = :id)', [
                'type' => 'artist',
                'id' => $id,
            ])
            //->orderBy('c.created_at DESC')
            ->all();

        return $this->render('artist', [
            'model' => $model,
            'list' => $list,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
