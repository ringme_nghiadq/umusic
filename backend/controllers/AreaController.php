<?php

namespace backend\controllers;

use backend\models\Area;
use common\helpers\Helpers;
use yii;

class AreaController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionGetDistrict() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $params = Yii::$app->request->post('depdrop_all_params');
        $data = [];

        if (isset($params['province-id'])) {
            $data = Area::getDistrictQuery($params['province-id'])
                ->select('district id, name')
                ->asArray()
                ->all();

            $data = array_values($data);

        }

        return ['output'=> $data, 'selected'=> Yii::$app->request->get('selected')];
    }

    public function actionGetPrecinct() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $params = Yii::$app->request->post('depdrop_all_params');
        $data = [];

        if (isset($params['province-id']) && isset($params['district-id'])) {
            $data = Area::getPrecinctQuery($params['province-id'], $params['district-id'])
                ->select('precinct id, name')
                ->asArray()
                ->all();

            $data = array_values($data);
        }

        return ['output'=> $data, 'selected'=> Yii::$app->request->get('selected')];
    }
}
