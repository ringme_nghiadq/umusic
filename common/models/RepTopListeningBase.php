<?php

namespace common\models;

use Yii;
class RepTopListeningBase extends \common\models\db\RepTopListeningDB {
    public function behaviors() {
        return [];
    }

    public function attributeLabels() {
        return [
            'song_id' => Yii::t('backend', 'Song'),
        ];
    }

    public function getSong() {
        return $this->hasOne(SongBase::className(), ['id' => 'song_id']);
    }
}