<?php

namespace common\models;

use common\helpers\Helpers;
use Yii;

class MscCountryBase extends \common\models\db\MscCountryDB
{


    public function behaviors()
    {
        return [
        ];
    }
    public static function getCountryArray() {
        $itemArr = self::find()
            ->select('country_code, country_name')
            ->asArray()
            ->orderBy('country_name asc')
            ->all();

        if (!empty($itemArr)) {
            return  \yii\helpers\ArrayHelper::map($itemArr, 'country_code', 'country_name');

        } else {
            return array();
        }
    }

    public function getCategories() {
        return $this->hasMany(CategoryBase::className(), ['id' => 'cate_id'])
            ->viaTable('msc_category_country', ['country_code' => 'country_code']);
    }

    public function getAvatarUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->flag_image);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->flag_image, $w, $h));
        }

    }

}