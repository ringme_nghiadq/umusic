<?php

namespace common\models;

use common\helpers\Helpers;
use Yii;
use yii\behaviors\SluggableBehavior;
use backend\components\behaviors\UnicodeSluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class PlaylistBase extends \common\models\db\PlaylistDB
{
    const STATUS_DELETE = -1;
    const STATUS_DRAFT = 0;
    const STATUS_APPROVED = 1;

    public function behaviors()
    {
        return [
            [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'title',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getAvatarUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->avatar);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->avatar, $w, $h));
        }

    }

    public function getThumbnailUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->thumbnail);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->thumbnail, $w, $h));
        }

    }

    public function getBannerUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->banner);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->banner, $w, $h));
        }

    }

    public function getSongs() {
        return $this->hasMany(SongBase::className(), ['id' => 'song_id'])
            ->viaTable('msc_song_playlist', ['playlist_id' => 'id']);
    }
}