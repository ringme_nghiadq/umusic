<?php

namespace common\models;

use Yii;
class RepTopDownloadingBase extends \common\models\db\RepTopDownloadingDB {
    public function behaviors() {
        return [];
    }

    public function attributeLabels() {
        return [
            'song_id' => Yii::t('backend', 'Song'),
        ];
    }

    public function getSong() {
        return $this->hasOne(SongBase::className(), ['id' => 'song_id']);
    }
}