<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class SongCategoryBase extends \common\models\db\SongCategoryDB
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function getSong() {
        return $this->hasOne(SongBase::className(), ['id' => 'song_id']);
    }
}