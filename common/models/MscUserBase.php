<?php

namespace common\models;

use common\helpers\Helpers;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class MscUserBase extends \common\models\db\MscUserDB
{

    const STATUS_DELETE = -1;
    const STATUS_DRAFT = 0;
    const STATUS_APPROVED = 1;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function getLastAvatarUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->last_avatar);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->last_avatar, $w, $h));
        }

    }
}