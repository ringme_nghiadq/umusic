<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use common\helpers\Helpers;

class MscCollectionBase extends \common\models\db\MscCollectionDB
{
    const STATUS_DELETE = -1;
    const STATUS_DRAFT = 0;
    const STATUS_APPROVED = 1;
    public $album;
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function getCollectionArray() {
        $itemArr = self::find()
            ->select('id, collection_name')
            ->where(['is_active' => '1'])
            ->asArray()
            ->orderBy('collection_name asc')
            ->all();

        if (!empty($itemArr)) {
            return  \yii\helpers\ArrayHelper::map($itemArr, 'id', 'collection_name');

        } else {
            return array();
        }
    }

    public function getAlbums() {
        return $this->hasMany(AlbumBase::className(), ['id' => 'content_id'])
            ->viaTable('msc_collection_content', ['collection_id' => 'id', 'content_type' => 'album'])

        ;
    }

    public function getPlaylists() {
        return $this->hasMany(PlaylistBase::className(), ['id' => 'content_id'])
            ->viaTable('msc_collection_content', ['collection_id' => 'id', 'content_type' => 'playlist']);
    }

    public function getCategories() {
        return $this->hasMany(MscCategoryBase::className(), ['id' => 'content_id'])
            ->viaTable('msc_collection_content', ['collection_id' => 'id', 'content_type' => 'category']);
    }

    public function getContents() {
        return $this->hasMany(MscCollectionContentBase::className(), ['collection_id' => 'id']);
    }
}