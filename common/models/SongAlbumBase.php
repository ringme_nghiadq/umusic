<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class SongAlbumBase extends \common\models\db\SongAlbumDB
{


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $albums = $this->albums;
        foreach ($albums as $album) {
            $album->recalculateSongs();
            $album->save(false, ['number_of_songs']);
        }
        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert) {
            $albums = $this->albums;
            foreach ($albums as $album) {
                $album->recalculateSongs();
                $album->save(false, ['number_of_songs']);
            }
        }
    }

    public function getAlbums() {
        return $this->hasMany(AlbumBase::className(), ['id' => 'album_id']);
    }
}