<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class MscCategoryCountryBase extends \common\models\db\MscCategoryCountryDB
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function getCategory() {
        return $this->hasOne(MscCategoryBase::className(), ['id' => 'cate_id']);
    }
}