<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class ApiLoginAuthBase extends \common\models\db\ApiLoginAuthDB
{


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
                'createdAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }


}