<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;
use yii\helpers\Html;
use common\helpers\Helpers;

class MscCategoryContentBase extends common\models\db\MscCategoryContentDB{
    const CONTENT_TYPE_PLAYLIST = 'playlist';
    const CONTENT_TYPE_ALBUM = 'album';
    const CONTENT_TYPE_SONG = 'song';
    const CONTENT_TYPE_ARTIST = 'artist';

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function attributeLabels() {
        return [
            'category_id' => Yii::t('backend', 'Category'),
            'content_id' => Yii::t('backend', 'Content'),
        ];
    }

    public function getAlbum() {
        return $this->hasOne(AlbumBase::className(), ['id' => 'content_id']);
    }

    public function getCategory() {
        return $this->hasOne(SongBase::className(), ['id' => 'content_id']);
    }

    public function getPlaylist() {
        return $this->hasOne(PlaylistBase::className(), ['id' => 'content_id']);
    }

    public function getArtist() {
        return $this->hasOne(ArtistBase::className(), ['id' => 'content_id']);
    }

    public function getContentName() {
        $contentName = null;
        /*switch ($this->content_type) {
            case self::CONTENT_TYPE_PLAYLIST:
                $model = PlaylistBase::findOne($this->content_id);
                $contentName = $model ? $model->playlist_name : $contentName;
                break;
            case self::CONTENT_TYPE_ALBUM:
                $model = AlbumBase::findOne($this->content_id);
                $contentName = $model ? $model->album_name : $contentName;
                break;
            case self::CONTENT_TYPE_CATEGORY:
                $model = MscCategoryBase::findOne($this->content_id);
                $contentName = $model ? $model->cate_name : $contentName;
                break;
        }*/
        if($this->playlist) {
            $contentName = $this->playlist->playlist_name;
        } elseif($this->album) {
            $contentName = $this->album->album_name;
        } elseif($this->category) {
            $contentName = $this->song->cate_name;
        } elseif($this->category) {
            $contentName = $this->artist->cate_name;
        }

        return $contentName;
    }

    public function getThumbnailUrl($w = null, $h = null) {
        $thumbnailUrl = null;
        switch ($this->content_type) {
            case self::CONTENT_TYPE_PLAYLIST:
                $thumbnailUrl = $this->playlist->getThumbnailUrl($w, $h);
                break;
            case self::CONTENT_TYPE_ALBUM:
                $thumbnailUrl = $this->album->getThumbnailUrl($w, $h);
                break;
            case self::CONTENT_TYPE_SONG:
                $thumbnailUrl = $this->song->getAvatarUrl($w, $h);
                break;
            case self::CONTENT_TYPE_ARTIST:
                $thumbnailUrl = $this->artist->getAvatarUrl($w, $h);
                break;
        }
        return $thumbnailUrl;
    }

    public function getFrontendDetailUrl() {
        $url = null;
        switch ($this->content_type) {
            case self::CONTENT_TYPE_PLAYLIST:
                $url = Url::to(['media/playlist-detail', 'id' => $this->playlist->id]);
                break;
            case self::CONTENT_TYPE_ALBUM:
                $url = Url::to(['media/album-detail', 'id' => $this->album->id]);
                break;
            case self::CONTENT_TYPE_SONG:
                $url = Url::to(['media/song-detail', 'id' => $this->song->id]);
                break;
            case self::CONTENT_TYPE_ARTIST:
                $url = Url::to(['media/artist-detail', 'id' => $this->artist->id]);
                break;
        }
        return $url;
    }
}
