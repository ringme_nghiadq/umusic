<?php

namespace common\models;

use common\helpers\Helpers;
use Yii;
use yii\behaviors\SluggableBehavior;
use backend\components\behaviors\UnicodeSluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class ArtistBase extends \common\models\db\ArtistDB
{
    const STATUS_DELETE = -1;
    const STATUS_DRAFT = 0;
    const STATUS_APPROVED = 1;



    public function getAvatarUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->avatar);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->avatar, $w, $h));
        }

    }

    public function getThumbnailUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->thumbnail);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->thumbnail, $w, $h));
        }

    }

    public function getBannerUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->banner);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->banner, $w, $h));
        }

    }

    public function behaviors()
    {
        return [
            [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'alias_name',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getCountry()
    {
        return $this->hasOne(MscCountryBase::className(), ['country_code' => 'country_code']);
    }

    public static function getAllGenders() {
        return [
            'male' => Yii::t('backend', 'Nam'),
            'female' => Yii::t('backend', 'Nữ'),
            'other' => Yii::t('backend', 'Khác'),
        ];
    }

    public function getSongs() {
        return $this->hasMany(SongBase::className(), ['id' => 'song_id'])
            ->viaTable('msc_song_artist', ['artist_id' => 'id']);
    }

    public function getDisplayGender() {
        $allGenders = self::getAllGenders();
        return in_array($this->gender, array_keys($allGenders)) ? $allGenders[$this->gender] : $this->gender;
    }
}