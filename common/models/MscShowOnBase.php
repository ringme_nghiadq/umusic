<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class MscShowOnBase extends \common\models\db\MscShowOnDB
{
    const STATUS_DELETE = -1;
    const STATUS_DRAFT = 0;
    const STATUS_APPROVED = 1;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }


}