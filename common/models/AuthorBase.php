<?php

namespace common\models;

use common\helpers\Helpers;
use Yii;
use yii\behaviors\SluggableBehavior;
use backend\components\behaviors\UnicodeSluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class AuthorBase extends \common\models\db\AuthorDB
{
    const STATUS_DELETE = -1;
    const STATUS_DRAFT = 0;
    const STATUS_APPROVED = 1;

    public function getAvatarUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->avatar);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->avatar, $w, $h));
        }

    }

    public function getThumbnailUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->thumbnail);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->thumbnail, $w, $h));
        }

    }

    public function getCountry()
    {
        return $this->hasOne(MscCountryBase::className(), ['country_code' => 'country_code']);
    }

    public function behaviors()
    {
        return [
            [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'alias_name',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }


}