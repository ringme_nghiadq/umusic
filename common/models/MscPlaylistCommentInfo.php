<?php

namespace common\models;

use Yii;

/**
 * This is the model class for collection "msc_playlist_comment_info".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $commentId
 * @property mixed $userId
 * @property mixed $name
 * @property mixed $contentId
 * @property mixed $content
 * @property mixed $commentAt
 * @property mixed $total_liked
 */
class MscPlaylistCommentInfo extends \yii\mongodb\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'msc_playlist_comment_info';
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return [
            '_id',
            'commentId',
            'userId',
            'name',
            'contentId',
            'content',
            'commentAt',
            'total_liked',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['commentId', 'userId', 'name', 'contentId', 'content', 'commentAt', 'total_liked'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'commentId' => 'Comment ID',
            'userId' => 'User ID',
            'name' => 'Name',
            'contentId' => 'Content ID',
            'content' => 'Content',
            'commentAt' => 'Comment At',
            'total_liked' => 'Total Liked',
        ];
    }
}
