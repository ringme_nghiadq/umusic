<?php

namespace common\models;

use common\helpers\Helpers;
use Yii;
use yii\behaviors\SluggableBehavior;
use backend\components\behaviors\UnicodeSluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class MscCategoryBase extends \common\models\db\MscCategoryDB
{

    const STATUS_DELETE = -1;
    const STATUS_DRAFT = 0;
    const STATUS_APPROVED = 1;

    public static function getCategoryArray() {
        $itemArr = self::find()
            ->select('id, cate_name')
            ->asArray()
            ->orderBy('cate_name asc')
            ->all();

        if (!empty($itemArr)) {
            return  \yii\helpers\ArrayHelper::map($itemArr, 'id', 'cate_name');

        } else {
            return array();
        }
    }

    public function getAvatarUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->avatar);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->avatar, $w, $h));
        }

    }

    public function getBannerUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->banner);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->banner, $w, $h));
        }

    }

    public function behaviors()
    {
        return [
            [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'cate_name',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getSongs() {
        return $this->hasMany(SongBase::className(), ['id' => 'song_id'])
            ->viaTable('msc_song_category', ['cate_id' => 'id']);
    }

    public function getSongsCategory() {
        return $this->hasMany(SongCategoryBase::className(), ['cate_id' => 'id']);
    }

    public function getCountry() {
        return $this->hasOne(MscCountryBase::className(), ['country_code' => 'country_code'])
            ->viaTable('msc_category_country', ['cate_id' => 'id']);
    }
}