<?php

namespace common\models;

use common\helpers\Helpers;
use Yii;
use yii\behaviors\SluggableBehavior;
use backend\components\behaviors\UnicodeSluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class SongBase extends \common\models\db\SongDB
{
    const STATUS_DELETE = -1;
    const STATUS_DRAFT = 0;
    const STATUS_APPROVED = 1;

    public function getMediaPathUrl() {
        if (strpos($this->media_path, 'http') === 0) {
            return $this->media_path;
        } elseif (strpos($this->media_path, '/cms_upload/') === 0) {
            return Yii::$app->params['media_url_cms']. $this->media_path;
        } elseif (strpos($this->media_path, '/usr_upload/') === 0) {
            return Yii::$app->params['media_url_user']. $this->media_path;
        } elseif (strpos($this->media_path, '/vcs_medias/') === 0) {
            return Yii::$app->params['media_url_vcs']. $this->media_path;
        } else {
            return Yii::$app->params['media_url']. $this->media_path;
        }
    }

    public function getAvatarUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->avatar);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->avatar, $w, $h));
        }

    }

    public function getThumbnailUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->thumbnail);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->thumbnail, $w, $h));
        }

    }

    public function getBannerUrl($w = null, $h = null) {
        if (!$w && !$h) {
            return Helpers::getMediaUrl($this->banner);
        } else {
            return Helpers::getMediaUrl(Helpers::getThumbUrl($this->banner, $w, $h));
        }

    }

    public function behaviors()
    {
        return [
            [
                'class' => UnicodeSluggableBehavior::className(),
                'attribute' => 'song_name',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getAuthor()
    {
        return $this->hasOne(AuthorBase::className(), ['id' => 'author_id']);
    }

    public function getCountry()
    {
        return $this->hasOne(MscCountryBase::className(), ['country_code' => 'country_code']);
    }

    public function getArtists() {
        return $this->hasMany(ArtistBase::className(), ['id' => 'artist_id'])->viaTable('msc_song_artist', ['song_id' => 'id']);
    }

    public function getSongArtists() {
        return $this->hasMany(SongArtistBase::className(), ['song_id' => 'id']);
    }

    public function getCategory() {
        return $this->hasOne(MscCategoryBase::className(), ['id' => 'cate_id'])
            ->viaTable('msc_song_category', ['song_id' => 'id']);
    }
}