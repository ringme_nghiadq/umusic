<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "rep_top_downloading".
 *
 * @property integer $id
 * @property string $report_date
 * @property integer $song_id
 * @property integer $total_download
 */
class RepTopDownloadingDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rep_top_downloading';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_date'], 'safe'],
            [['song_id', 'total_download'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_date' => 'Report Date',
            'song_id' => 'Song ID',
            'total_download' => 'Total Download',
        ];
    }
}
