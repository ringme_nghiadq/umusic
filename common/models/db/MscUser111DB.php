<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_user".
 *
 * @property int $id so dien thoai
 * @property string|null $user_id
 * @property string|null $login_id
 * @property string|null $base_on phone|email
 * @property string|null $phone_number
 * @property string|null $email
 * @property string $password
 * @property string|null $token
 * @property string|null $user_name
 * @property float|null $last_avatar thời gian thay đổi avatar cuối cùng - milisecond
 * @property int|null $active 1: đã login>1 lần,0: sinh pass chưa login,2:deactive
 * @property string|null $token_expired thời gian hết hạn của token, tính bằng milisecond
 * @property string|null $created_at
 * @property string|null $revision
 * @property string|null $client_type loai client
 * @property string|null $device_id
 * @property string|null $device_name
 * @property string|null $app_version
 * @property string|null $os_version
 * @property string|null $uuid
 * @property string|null $regid
 * @property string|null $v_regid
 * @property string|null $last_login
 * @property string|null $otp
 * @property string|null $otp_expired
 * @property string|null $link_reset_password
 * @property string|null $token_reset_password_expired
 */
class MscUser111DB extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'msc_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['password'], 'required'],
            [['last_avatar'], 'number'],
            [['active'], 'integer'],
            [['token_expired', 'created_at', 'last_login', 'otp_expired', 'token_reset_password_expired'], 'safe'],
            [['user_id', 'login_id'], 'string', 'max' => 30],
            [['base_on'], 'string', 'max' => 10],
            [['phone_number'], 'string', 'max' => 11],
            [['email'], 'string', 'max' => 45],
            [['password', 'token'], 'string', 'max' => 100],
            [['user_name'], 'string', 'max' => 250],
            [['revision', 'client_type'], 'string', 'max' => 7],
            [['device_id', 'app_version', 'uuid'], 'string', 'max' => 50],
            [['device_name', 'os_version'], 'string', 'max' => 150],
            [['regid', 'v_regid'], 'string', 'max' => 2000],
            [['otp'], 'string', 'max' => 200],
            [['link_reset_password'], 'string', 'max' => 500],
            [['user_id'], 'unique'],
            [['login_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'login_id' => 'Login ID',
            'base_on' => 'Base On',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'password' => 'Password',
            'token' => 'Token',
            'user_name' => 'User Name',
            'last_avatar' => 'Last Avatar',
            'active' => 'Active',
            'token_expired' => 'Token Expired',
            'created_at' => 'Created At',
            'revision' => 'Revision',
            'client_type' => 'Client Type',
            'device_id' => 'Device ID',
            'device_name' => 'Device Name',
            'app_version' => 'App Version',
            'os_version' => 'Os Version',
            'uuid' => 'Uuid',
            'regid' => 'Regid',
            'v_regid' => 'V Regid',
            'last_login' => 'Last Login',
            'otp' => 'Otp',
            'otp_expired' => 'Otp Expired',
            'link_reset_password' => 'Link Reset Password',
            'token_reset_password_expired' => 'Token Reset Password Expired',
        ];
    }
}
