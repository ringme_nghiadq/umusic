<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_country".
 *
 * @property integer $id
 * @property string $country_code
 * @property string $country_name
 * @property string $country_name_en
 * @property string $flag_image
 */
class MscCountryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_code'], 'string', 'max' => 10],
            [['country_name', 'country_name_en'], 'string', 'max' => 255],
            [['flag_image'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'country_code' => Yii::t('backend', 'Country Code'),
            'country_name' => Yii::t('backend', 'Country Name'),
            'country_name_en' => Yii::t('backend', 'Country Name En'),
            'flag_image' => Yii::t('backend', 'Flag Image'),
        ];
    }
}
