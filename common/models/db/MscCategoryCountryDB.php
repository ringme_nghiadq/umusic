<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_song_category".
 *
 * @property integer $id
 * @property string $country_code
 * @property integer $cate_id
 * @property string $created_at
 */
class MscCategoryCountryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_category_country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [ 'cate_id', 'integer'],
            [['country_code', 'cate_id'], 'required'],
            [['created_at'], 'safe'],
            [['country_code'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'country_code' => Yii::t('backend', 'Country Code'),
            'cate_id' => Yii::t('backend', 'Cate ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
