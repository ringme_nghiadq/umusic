<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_kw_sensitive_keyword".
 *
 * @property integer $id
 * @property string $keyword
 * @property string $created_at
 */
class MscKwSensitiveKeywordDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_kw_sensitive_keyword';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['keyword'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyword' => 'Keyword',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At'
        ];
    }
}
