<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_user".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $login_id
 * @property string $base_on
 * @property string $phone_number
 * @property string $email
 * @property string $password
 * @property string $token
 * @property string $user_name
 * @property string $last_avatar
 * @property integer $active
 * @property string $token_expired
 * @property string $created_at
 * @property string $revision
 * @property string $client_type
 * @property string $device_id
 * @property string $device_name
 * @property string $app_version
 * @property string $os_version
 * @property string $uuid
 * @property string $regid
 * @property string $v_regid
 * @property string $last_login
 * @property string $otp
 * @property string $otp_expired
 * @property string $link_reset_password
 * @property string $token_reset_password_expired
 */
class MscUserDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password'], 'required'],
            [['last_avatar'], 'number'],
            [['token_expired', 'created_at', 'last_login', 'otp_expired', 'token_reset_password_expired'], 'safe'],
            [['user_id', 'login_id'], 'string', 'max' => 30],
            [['base_on'], 'string', 'max' => 10],
            [['phone_number'], 'string', 'max' => 11],
            [['email'], 'string', 'max' => 45],
            [['password', 'token'], 'string', 'max' => 100],
            [['user_name'], 'string', 'max' => 250],
            [['active'], 'string', 'max' => 4],
            [['revision', 'client_type'], 'string', 'max' => 7],
            [['device_id', 'app_version', 'uuid'], 'string', 'max' => 50],
            [['device_name', 'os_version'], 'string', 'max' => 150],
            [['regid', 'v_regid'], 'string', 'max' => 2000],
            [['otp'], 'string', 'max' => 200],
            [['link_reset_password'], 'string', 'max' => 500],
            [['user_id'], 'unique'],
            [['login_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'login_id' => Yii::t('backend', 'Login ID'),
            'base_on' => Yii::t('backend', 'Base On'),
            'phone_number' => Yii::t('backend', 'Phone Number'),
            'email' => Yii::t('backend', 'Email'),
            'password' => Yii::t('backend', 'Password'),
            'token' => Yii::t('backend', 'Token'),
            'user_name' => Yii::t('backend', 'User Name'),
            'last_avatar' => Yii::t('backend', 'Last Avatar'),
            'active' => Yii::t('backend', 'Active'),
            'token_expired' => Yii::t('backend', 'Token Expired'),
            'created_at' => Yii::t('backend', 'Created At'),
            'revision' => Yii::t('backend', 'Revision'),
            'client_type' => Yii::t('backend', 'Client Type'),
            'device_id' => Yii::t('backend', 'Device ID'),
            'device_name' => Yii::t('backend', 'Device Name'),
            'app_version' => Yii::t('backend', 'App Version'),
            'os_version' => Yii::t('backend', 'Os Version'),
            'uuid' => Yii::t('backend', 'Uuid'),
            'regid' => Yii::t('backend', 'Regid'),
            'v_regid' => Yii::t('backend', 'V Regid'),
            'last_login' => Yii::t('backend', 'Last Login'),
            'otp' => Yii::t('backend', 'Otp'),
            'otp_expired' => Yii::t('backend', 'Otp Expired'),
            'link_reset_password' => Yii::t('backend', 'Link Reset Password'),
            'token_reset_password_expired' => Yii::t('backend', 'Token Reset Password Expired'),
        ];
    }
}
