<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_show_on".
 *
 * @property integer $id
 * @property string $position_key
 * @property string $position_name
 * @property integer $content_id
 * @property string $content_type
 * @property string $note
 * @property string $updated_at
 * @property string $created_at
 */
class MscShowOnDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_show_on';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content_id'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['position_key', 'position_name'], 'string', 'max' => 100],
            [['content_type'], 'string', 'max' => 20],
            [['note'], 'string', 'max' => 250]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'position_key' => Yii::t('backend', 'Position Key'),
            'position_name' => Yii::t('backend', 'Position Name'),
            'content_id' => Yii::t('backend', 'Content ID'),
            'content_type' => Yii::t('backend', 'Content Type'),
            'note' => Yii::t('backend', 'Note'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
