<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_collection_content".
 *
 * @property integer $id
 * @property integer $collection_id
 * @property integer $content_id
 * @property string $content_type
 * @property string $created_at
 */
class MscCollectionContentDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_collection_content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['collection_id', 'content_id'], 'required'],
            [['collection_id', 'content_id'], 'integer'],
            [['created_at'], 'safe'],
            [['content_type'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'collection_id' => Yii::t('backend', 'Collection ID'),
            'content_id' => Yii::t('backend', 'Content ID'),
            'content_type' => Yii::t('backend', 'Content Type'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
