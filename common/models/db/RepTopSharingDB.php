<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "rep_top_sharing".
 *
 * @property integer $id
 * @property string $report_date
 * @property integer $song_id
 * @property integer $total_shared
 */
class RepTopSharingDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rep_top_sharing';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_date'], 'safe'],
            [['song_id', 'total_shared'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'report_date' => 'Report Date',
            'song_id' => 'Song ID',
            'total_shared' => 'Total Shared',
        ];
    }
}
