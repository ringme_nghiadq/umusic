<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_song".
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $country_code
 * @property string $song_name
 * @property string $avatar
 * @property string $banner
 * @property string $thumbnail
 * @property string $media_path
 * @property string $lyric
 * @property integer $is_active
 * @property integer $bit_rate
 * @property integer $duration
 * @property string $slug
 * @property integer $is_download
 * @property integer $total_listens
 * @property integer $total_download
 * @property integer $total_liked
 * @property integer $total_shared
 * @property integer $total_comment
 * @property integer $total_score
 * @property integer $number_of_artists
 * @property string $published_time
 * @property string $updated_at
 * @property string $created_at
 */
class SongDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_song';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'bit_rate', 'duration', 'is_download', 'total_listens', 'total_download', 'total_liked', 'total_shared', 'total_comment', 'total_score', 'number_of_artists'], 'integer'],
            [['country_code', 'song_name', 'created_at'], 'required'],
            [['lyric'], 'string'],
            [['published_time', 'updated_at', 'created_at'], 'safe'],
            [['country_code'], 'string', 'max' => 10],
            [['song_name', 'media_path', 'slug'], 'string', 'max' => 255],
            [['avatar', 'banner', 'thumbnail'], 'string', 'max' => 500],
            [['is_active'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'author_id' => Yii::t('backend', 'Author'),
            'country_code' => Yii::t('backend', 'Country Code'),
            'song_name' => Yii::t('backend', 'Song Name'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'banner' => Yii::t('backend', 'Banner'),
            'thumbnail' => Yii::t('backend', 'Thumbnail'),
            'media_path' => Yii::t('backend', 'Media Path'),
            'lyric' => Yii::t('backend', 'Lyric'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'bit_rate' => Yii::t('backend', 'Bit Rate'),
            'duration' => Yii::t('backend', 'Duration'),
            'slug' => Yii::t('backend', 'Slug'),
            'is_download' => Yii::t('backend', 'Is Download'),
            'total_listens' => Yii::t('backend', 'Total Listens'),
            'total_download' => Yii::t('backend', 'Total Download'),
            'total_liked' => Yii::t('backend', 'Total Liked'),
            'total_shared' => Yii::t('backend', 'Total Shared'),
            'total_comment' => Yii::t('backend', 'Total Comment'),
            'total_score' => Yii::t('backend', 'Total Score'),
            'number_of_artists' => Yii::t('backend', 'Number Of Artists'),
            'published_time' => Yii::t('backend', 'Published Time'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
