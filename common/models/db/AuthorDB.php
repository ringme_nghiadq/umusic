<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_author".
 *
 * @property integer $id
 * @property string $country_code
 * @property string $alias_name
 * @property string $real_name
 * @property string $avatar
 * @property string $thumbnail
 * @property integer $is_active
 * @property string $info
 * @property string $birthday
 * @property string $gender
 * @property string $address
 * @property string $slug
 * @property string $updated_at
 * @property string $created_at
 */
class AuthorDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_author';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_code', 'alias_name', 'created_at'], 'required'],
            [['info'], 'string'],
            [['birthday', 'updated_at', 'created_at'], 'safe'],
            [['country_code'], 'string', 'max' => 10],
            [['alias_name', 'real_name', 'address', 'slug'], 'string', 'max' => 255],
            [['avatar', 'thumbnail'], 'string', 'max' => 500],
            [['is_active'], 'string', 'max' => 1],
            [['gender'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'country_code' => Yii::t('backend', 'Country Code'),
            'alias_name' => Yii::t('backend', 'Alias Name'),
            'real_name' => Yii::t('backend', 'Real Name'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'thumbnail' => Yii::t('backend', 'Thumbnail'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'info' => Yii::t('backend', 'Info'),
            'birthday' => Yii::t('backend', 'Birthday'),
            'gender' => Yii::t('backend', 'Gender'),
            'address' => Yii::t('backend', 'Address'),
            'slug' => Yii::t('backend', 'Slug'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
