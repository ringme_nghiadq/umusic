<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_kw_top_keyword".
 *
 * @property integer $id
 * @property string $keyword
 * @property string $created_at
 */
class MscKwTopKeywordDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_kw_top_keyword';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['keyword'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keyword' => 'Keyword',
            'created_at' => 'Created At',
        ];
    }
}
