<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_category".
 *
 * @property integer $id
 * @property string $cate_name
 * @property string $avatar
 * @property string $banner
 * @property string $description
 * @property integer $is_active
 * @property string $slug
 * @property string $updated_at
 * @property string $created_at
 */
class MscCategoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cate_name', 'is_active', 'created_at'], 'required'],
            [['description'], 'string'],
            [['is_active'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['cate_name', 'slug'], 'string', 'max' => 255],
            [['avatar', 'banner'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'cate_name' => Yii::t('backend', 'Cate Name'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'banner' => Yii::t('backend', 'Banner'),
            'description' => Yii::t('backend', 'Description'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'slug' => Yii::t('backend', 'Slug'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
