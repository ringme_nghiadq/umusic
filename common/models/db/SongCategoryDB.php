<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_song_category".
 *
 * @property integer $id
 * @property integer $song_id
 * @property integer $cate_id
 * @property string $created_at
 */
class SongCategoryDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_song_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['song_id', 'cate_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'song_id' => Yii::t('backend', 'Song ID'),
            'cate_id' => Yii::t('backend', 'Cate ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
