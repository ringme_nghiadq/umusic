<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "partner".
 *
 * @property integer $id
 * @property string $name
 * @property string $ruc
 * @property string $director
 * @property string $director_dni
 * @property string $address
 * @property string $description
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property BtsPlanDB[] $btsPlans
 */
class PartnerDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'ruc'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'ruc'], 'string', 'max' => 200],
            [['director', 'director_dni', 'address'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            [['status'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'ruc' => Yii::t('backend', 'Ruc'),
            'director' => Yii::t('backend', 'Director'),
            'director_dni' => Yii::t('backend', 'Director Dni'),
            'address' => Yii::t('backend', 'Address'),
            'description' => Yii::t('backend', 'Description'),
            'status' => Yii::t('backend', 'Status'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBtsPlans()
    {
        return $this->hasMany(BtsPlanDB::className(), ['partner_id' => 'id']);
    }
}
