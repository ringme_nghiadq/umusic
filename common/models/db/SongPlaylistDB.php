<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_song_playlist".
 *
 * @property integer $id
 * @property integer $song_id
 * @property integer $playlist_id
 * @property string $created_at
 */
class SongPlaylistDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_song_playlist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['song_id', 'playlist_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'song_id' => Yii::t('backend', 'Song ID'),
            'playlist_id' => Yii::t('backend', 'Playlist ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
