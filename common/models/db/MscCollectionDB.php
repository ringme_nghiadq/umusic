<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_collection".
 *
 * @property integer $id
 * @property string $collection_name
 * @property string $description
 * @property integer $is_active
 * @property integer $number_of_artists
 * @property string $note
 * @property string $updated_at
 * @property string $created_at
 */
class MscCollectionDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_collection';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['collection_name', 'is_active', 'created_at'], 'required'],
            [['description'], 'string'],
            [['is_active', 'number_of_artists'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['collection_name'], 'string', 'max' => 255],
            [['note'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'collection_name' => Yii::t('backend', 'Collection Name'),
            'description' => Yii::t('backend', 'Description'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'number_of_artists' => Yii::t('backend', 'Number Of Artists'),
            'note' => Yii::t('backend', 'Note'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
