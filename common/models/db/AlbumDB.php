<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_album".
 *
 * @property integer $id
 * @property string $country_code
 * @property string $album_name
 * @property string $avatar
 * @property string $banner
 * @property string $thumbnail
 * @property string $description
 * @property integer $is_active
 * @property integer $number_of_songs
 * @property string $slug
 * @property integer $total_listens
 * @property integer $total_download
 * @property integer $total_liked
 * @property integer $total_shared
 * @property integer $total_comment
 * @property double $boost_score
 * @property integer $number_of_artists
 * @property string $published_time
 * @property string $updated_at
 * @property string $created_at
 */
class AlbumDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['album_name', 'created_at'], 'required'],
            [['description', 'boost_score'], 'string'],
            [['number_of_songs', 'total_listens', 'total_download', 'total_liked', 'total_shared', 'total_comment', 'number_of_artists'], 'integer'],
            [['published_time', 'updated_at', 'created_at'], 'safe'],
            [['country_code'], 'string', 'max' => 10],
            [['album_name', 'avatar', 'banner', 'thumbnail'], 'string', 'max' => 500],
            [['is_active'], 'string', 'max' => 1],
            [['slug'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'country_code' => Yii::t('backend', 'Country Code'),
            'album_name' => Yii::t('backend', 'Album Name'),
            'avatar' => Yii::t('backend', 'Avatar'),
            'banner' => Yii::t('backend', 'Banner'),
            'thumbnail' => Yii::t('backend', 'Thumbnail'),
            'description' => Yii::t('backend', 'Description'),
            'is_active' => Yii::t('backend', 'Is Active'),
            'number_of_songs' => Yii::t('backend', 'Number Of Songs'),
            'slug' => Yii::t('backend', 'Slug'),
            'total_listens' => Yii::t('backend', 'Total Listens'),
            'total_download' => Yii::t('backend', 'Total Download'),
            'total_liked' => Yii::t('backend', 'Total Liked'),
            'total_shared' => Yii::t('backend', 'Total Shared'),
            'total_comment' => Yii::t('backend', 'Total Comment'),
            'boost_score' => Yii::t('backend', 'Boost Score'),
            'number_of_artists' => Yii::t('backend', 'Number Of Artists'),
            'published_time' => Yii::t('backend', 'Published Time'),
            'updated_at' => Yii::t('backend', 'Updated At'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
