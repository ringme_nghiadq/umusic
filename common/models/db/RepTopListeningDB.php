<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "rep_top_listening".
 *
 * @property integer $id
 * @property string $report_date
 * @property integer $song_id
 * @property integer $total_listens
 */
class RepTopListeningDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rep_top_listening';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_date'], 'safe'],
            [['song_id', 'total_listens'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'report_date' => Yii::t('backend', 'Report Date'),
            'song_id' => Yii::t('backend', 'Song ID'),
            'total_listens' => Yii::t('backend', 'Total Listens'),
        ];
    }
}
