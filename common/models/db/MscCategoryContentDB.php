<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_category_content".
 *
 * @property int $id
 * @property int $category_id
 * @property int $content_id
 * @property string|null $content_type
 * @property string|null $created_at
 */
class MscCategoryContentDB extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'msc_category_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'content_id'], 'required'],
            [['category_id', 'content_id'], 'integer'],
            [['created_at'], 'safe'],
            [['content_type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'content_id' => 'Content ID',
            'content_type' => 'Content Type',
            'created_at' => 'Created At',
        ];
    }
}
