<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_song_album".
 *
 * @property integer $id
 * @property integer $song_id
 * @property integer $album_id
 * @property string $created_at
 */
class SongAlbumDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_song_album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['song_id', 'album_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'song_id' => Yii::t('backend', 'Song ID'),
            'album_id' => Yii::t('backend', 'Album ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
