<?php

namespace common\models\db;

use Yii;

/**
 * This is the model class for table "msc_song_artist".
 *
 * @property integer $id
 * @property integer $song_id
 * @property integer $artist_id
 * @property string $created_at
 */
class SongArtistDB extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'msc_song_artist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['song_id', 'artist_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'song_id' => Yii::t('backend', 'Song ID'),
            'artist_id' => Yii::t('backend', 'Artist ID'),
            'created_at' => Yii::t('backend', 'Created At'),
        ];
    }
}
