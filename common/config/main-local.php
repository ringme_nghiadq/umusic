<?php

return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',

            'dsn' => 'mysql:host=103.143.206.63:3306;dbname=umusic',
            'username' => 'perudb',
            'password' => 'Perudb@2021',
//            'dsn' => 'mysql:host=localhost:3306;dbname=perudb',
//            'username' => 'root',
//            'password' => '',
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            // Duration of schema cache.
            'schemaCacheDuration' => 3600,
            // Name of the cache component used to store schema information
            'schemaCache' => 'cache',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
//            'class' => 'yii\redis\Cache',
//            'redis' => [
//                'hostname' => 'localhost',
//                'port' => 8002,
//                'database' => 1,
//            ]
        ],

        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            'decimalSeparator' => '.',
            'thousandSeparator' => ',',
        ],
        'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            'dsn' => 'mongodb://103.143.206.63:27017/umusic?authSource=admin',
            'options' => [
                "username" => "myUserAdmin",
                "password" => "9jzncDeqT676qZbU"
            ]
        ],
    ],
];
